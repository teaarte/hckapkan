<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchgoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchgoals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_id')->default(0)->nullable();
            $table->integer('matchteam_id')->default(0)->nullable();
            $table->integer('minute')->default(0)->nullable();
            $table->integer('throw_id')->default(0)->nullable();
            $table->integer('asist1_id')->default(0)->nullable();
            $table->integer('asist2_id')->default(0)->nullable();
            $table->string('throw')->default("")->nullable();
            $table->string('asist1')->default("")->nullable();
            $table->string('asist2')->default("")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matchgoals');
    }
}
