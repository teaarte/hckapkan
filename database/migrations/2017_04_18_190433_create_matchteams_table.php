<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchteamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchteams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_id')->default(0)->nullable();
            $table->timestamps();
        });
        Schema::create('matchteam_player', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id')->default(0)->nullable();
            $table->integer('matchteam_id')->default(0)->nullable();
            $table->integer('position')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matchteams');
        Schema::dropIfExists('matchteam_player');
    }
}
