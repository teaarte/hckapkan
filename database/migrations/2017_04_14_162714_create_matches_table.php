<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendar_id')->default(0)->nullable();
            $table->integer('team1_goals')->default(0)->nullable();
            $table->integer('team2_goals')->default(0)->nullable();
            $table->integer('team1_id')->default(0)->nullable();
            $table->integer('team2_id')->default(0)->nullable();
            $table->boolean('finished')->default(false)->nullable();
            $table->integer('photo_id')->default(0)->nullable();
            $table->integer('video_id')->default(0)->nullable();
            $table->integer('arena_id')->default(0)->nullable();
            $table->dateTime('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
