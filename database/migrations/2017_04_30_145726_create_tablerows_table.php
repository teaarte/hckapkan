<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablerowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tablerows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('table_id')->default(0);
            $table->string('col_0')->default("")->nullable();
            $table->string('col_1')->default("")->nullable();
            $table->string('col_2')->default("")->nullable();
            $table->string('col_3')->default("")->nullable();
            $table->string('col_4')->default("")->nullable();
            $table->string('col_5')->default("")->nullable();
            $table->string('col_6')->default("")->nullable();
            $table->string('col_7')->default("")->nullable();
            $table->string('col_8')->default("")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tablerows');
    }
}
