<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default("")->nullable();
            $table->string('surname')->default("")->nullable();
            $table->string('father_name')->default("")->nullable();
            $table->date('birthday')->nullable();
            $table->string('photo_url')->default("")->nullable();
            $table->float('height')->default(0)->nullable();
            $table->float('weight')->default(0)->nullable();
            $table->integer('position')->default(0)->nullable();
            $table->string('url_vk')->default("")->nullable();
            $table->string('url_fb')->default("")->nullable();
            $table->string('url_ok')->default("")->nullable();
            $table->string('url_spbhl')->default("")->nullable();
            $table->boolean('in_team')->default(false)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
