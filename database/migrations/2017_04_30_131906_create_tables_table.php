<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default("")->nullable();
            $table->string('url')->default("")->nullable();
            $table->integer('table_number')->default(0)->nullable();
            $table->boolean('active')->default(false)->nullable();
            $table->boolean('show_on_main')->default(false)->nullable();
            $table->integer('team_id')->default(0)->nullable();
            $table->integer('position')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tables');
    }
}
