<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'name',
        'logotype_url',
        'arena_id',
        'enemy'
    ];

    public function getArenaName() {
        $arena = $this->arena()->get()->first();
        if ($arena != null) return $arena['name'];
        return "";
    }

    public function players() {
        return $this->belongsToMany('App\Player');
    }

    public function arena() {
        return $this->belongsTo('App\Arena');
    }

    public function getMatchteams() {
        return $this->hasMany('App\Matchteam');
    }

    public function getImage() {
        if ($this['logotype_url'] != '') {
            return $this['logotype_url'];
        }
        return '/img/main/blank01.jpg';
    }
}
