<?php

namespace App\Console\Commands;

use App\Table;
use App\Tablerow;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Yangqi\Htmldom\Htmldom;

class TableParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'table:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse all tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tables = Table::all()->where('active', 1);
        foreach ($tables as $table) {
            $this->parse($table);
        }
    }

    public function parse(Table $tableObject) {
        $client = new Client();
        $response = $client->request('GET', $tableObject['url']);
        $data = $response->getBody()->getContents();

        $html = new Htmldom($data);

        $table_header = [];
        $table_rows = [];

        $_header = [];
        $_rows = [];

        $table_number = $tableObject['table_number'];
        $i = 1;
        foreach($html->find('table.standings') as $table) {
            if ($i == $table_number) {
                $rows = $table->find('tr');
                foreach ($rows[0]->find('th') as $cell) {
                    $_header[] = $cell->plaintext;
                }

                foreach ($table->find('tr') as $row) {
                    $cellData = [];
                    foreach ($row->find('td') as $cell) {
                        $cellData[] = $cell->plaintext;
                    }
                    if (count($cellData) > 0)
                        $_rows[] = $cellData;
                }
            }
            $i++;
        }

        if (count($_rows) > 0) {
            for ($i = 0; $i < count($_header); $i++) {
                if (!is_numeric($_header[$i])) {
                    $table_header[] = $_header[$i];
                }
            }
            $table_rows[] = $table_header;
            foreach ($_rows as $_row) {
                $row = [];
                for ($i = 0; $i < count($_row); $i++) {
                    if (!is_numeric($_header[$i])) {
                        $row[] = $_row[$i];
                    }
                }
                $table_rows[] = $row;
            }

            foreach ($tableObject->tablerows()->get() as $tablerow) {
                $tablerow->delete();
            }

            foreach ($table_rows as $table_row) {
                $fields = [];
                for ($i = 0; $i < count($table_row); $i++) {
                    $fields["col_".$i] = $table_row[$i];
                }
                $tablerow = Tablerow::create($fields);
                $tableObject->tablerows()->save($tablerow);
            }

        }
    }
}
