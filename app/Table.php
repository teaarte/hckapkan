<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $fillable = [
        'name',
        'url',
        'table_number',
        'active',
        'show_on_main',
        'team_id',
        'position'
    ];

    public function team() {
        return $this->belongsTo('App\Team');
    }

    public function tablerows() {
        return $this->hasMany('App\Tablerow');
    }

    public function getTeamName() {
        $team = $this->team()->get()->first();
        if ($team != null) {
            return $team['name'];
        }
        return "";
    }

    public function getHeaderRow() {
        $row = $this->tablerows()->get()->sortBy('id')->first();
        return $row;
    }

    public function getBodyRows() {
        $rows = $this->tablerows()->get()->sortBy('id')->slice(1);
        return $rows;
    }

}
