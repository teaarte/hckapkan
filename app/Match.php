<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class Match extends Model
{
    protected $fillable = [
        'calendar_id',
        'team1_goals',
        'team2_goals',
        'team1_id',
        'team2_id',
        'finished',
        'photo_id',
        'video_id',
        'arena_id',
        'date'
    ];

    public function getPhotoalbum() {
        return $this->belongsTo('App\Photoalbum', 'photo_id')->get()->first();
    }

    public function getVideo() {
        return $this->belongsTo('App\Video', 'video_id')->get()->first();
    }

    public function getMatchgoals() {
        return $this->hasMany('App\Matchgoal');
    }

    public function getTeamLogo($team) {
        $match_team = Matchteam::all()->where('id', $this['team'.$team.'_id'])->first();
        $team = $match_team->team()->get()->first();
        return $team->getImage();
    }

    public function getTeamName($team) {
        $match_team = Matchteam::all()->where('id', $this['team'.$team.'_id'])->first();
        $team = $match_team->team()->get()->first();
        return $team['name'];
    }

    public function getTeamArenaName($team) {
        $match_team = Matchteam::all()->where('id', $this['team'.$team.'_id'])->first();
        $team = $match_team->team()->get()->first();
        return $team->getArenaName();
    }

    public function getTeam($team) {
        $match_team = Matchteam::all()->where('id', $this['team'.$team.'_id'])->first();
        $team = $match_team->team()->get()->first();
        return $team;
    }

    public function getMatchteam($team) {
        $match_team = Matchteam::all()->where('id', $this['team'.$team.'_id'])->first();
        return $match_team;
    }

    public function getArenaName() {
        $arena = Arena::all()->where('id', $this['arena_id'])->first();
        return $arena['name'];
    }

    public function getStatusName() {
        switch ($this['finished']) {
            case 0:
                return "Результатов нет";
                break;
            case 1:
                return "Результаты записаны";
                break;
        }
        return "";
    }

    public function checkWinner($team) {
        if ($this['finished'] == 1) {
            $winner = 0;
            if ($this['team1_goals'] > $this['team2_goals']) $winner = 1;
            if ($this['team2_goals'] > $this['team1_goals']) $winner = 2;
            if ($winner == $team) {
                return true;
            }
        }
        return false;
    }


    public function getDate2() {
        $date = Carbon::parse($this['date']);
        return $date->format("Y-m-d H:i");
    }

    public function getDate() {
        Date::setLocale('ru');
        $date = Date::parse($this['date']);
        $name = $date->format("D, d.m.Y");;
        $first = mb_substr($name,0,1, 'UTF-8');//первая буква
        $last = mb_substr($name,1);//все кроме первой буквы
        $first = mb_strtoupper($first, 'UTF-8');
        $last = mb_strtolower($last, 'UTF-8');
        $name = $first.$last;
        return $name;
    }

    public function getTime() {
        $date = Carbon::parse($this['date']);
        return $date->format("H:i");
    }

}
