<?php

namespace App;

use Carbon\Carbon;
use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'title',
        'text',
        'images_json',
        'draft',
        'created_at',
        'photoalbum_id'
    ];

    public function photoalbum() {
        return $this->belongsTo('App\Photoalbum');
    }

    public function getDate() {
        $date = Carbon::parse($this['created_at']);
        return $date->format("d.m.Y");
    }

    public function getWordDate() {
        Date::setLocale('ru');
        return Date::parse($this['created_at'])->format('d M Y');
    }

    public function getUrl() {
        return route('news.show', [$this['id'], str_slug($this['title'],'-')]);
    }

    public function getTitle() {
        return $this['title'];
    }

    public function getShortText() {
        return str_limit(strip_tags($this['text']),200);
    }

    public function getText() {
        return $this['text'];
    }

    public function getPhoto() {
        $photoalbum = $this->photoalbum()->get()->first();
        if ($photoalbum == null) return '';
        return $photoalbum->getThumbail();
    }

    public function getImages() {
        $images = Collection::make();
        $photoalbum = $this->photoalbum()->get()->first();
        if ($photoalbum == null) return $images;

        foreach ($photoalbum->photos()->get() as $photo) {
            $images->add($photo['url']);
        }
        return $images;
    }

    public function getVis() {
        return 2;
    }
}
