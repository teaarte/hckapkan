<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stats extends Model
{
    protected $fillable = [
        'player_id',
        'team_id',
        'throw',
        'asist',
        'pass',
    ];
}
