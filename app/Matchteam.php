<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matchteam extends Model
{
    protected $fillable = [
        'team_id',
    ];

    public function players() {
        return $this->belongsToMany('App\Player');
    }

    public function team() {
        return $this->belongsTo('App\Team');
    }
}
