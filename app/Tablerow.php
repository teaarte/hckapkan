<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tablerow extends Model
{
    protected $fillable = [
        'table_id',
        'col_0',
        'col_1',
        'col_2',
        'col_3',
        'col_4',
        'col_5',
        'col_6',
        'col_7',
        'col_8',
    ];

    public function getString($index) {
        return $this['col_'.$index];
    }
}
