<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arena extends Model
{
    protected $fillable = [
        'name',
        'short_name',
        'address'
    ];
}
