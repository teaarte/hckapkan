<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'text',
        'visible_menu'
    ];

    public function getUrl() {
        return route('page.show', $this['slug']);
    }

    public function getTitle() {
        return $this['title'];
    }

    public function getText() {
        return $this['text'];
    }
}
