<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Player extends Model
{
    protected $fillable = [
        'name',
        'surname',
        'father_name',
        'birthday',
        'photo_url',
        'height',
        'weight',
        'position',
        'url_vk',
        'url_fb',
        'url_ok',
        'url_spbhl',
        'number',
        'in_team'
    ];

    public function teams() {
        return $this->belongsToMany('App\Team');
    }

    public function getStats() {
        $stats = [
            [
                'team_id' => 0,
                'team' => 'Общая статистика',
                'throw' => 0,
                'asist' => 0,
                'pass' => 0,
            ]

        ];
        foreach ($this->teams()->get() as $team) {
            $stat = $this->getStatsInTeam($team);
            $stats[0]['throw'] += $stat['throw'];
            $stats[0]['asist'] += $stat['asist'];
            $stats[0]['pass'] += $stat['pass'];
            array_push($stats, [
                'team_id' => $team['id'],
                'team' => $team['name'],
                'throw' => $stat['throw'],
                'asist' => $stat['asist'],
                'pass' => $stat['pass'],
            ]);
        }
        return $stats;
    }

    public function getStatsInTeam(Team $team) {
        $matchteams = $team->getMatchteams()->get();
        $results = [
            "throw" => 0,
            "asist1" => 0,
            "asist2" => 0,
            "pass" => 0,
        ];
        foreach ($matchteams as $matchteam) {
            $matchgoals = Matchgoal::all()->where('matchteam_id', $matchteam['id']);
            $fields = [ 'throw', 'asist1', 'asist2'];

            foreach ($fields as $field) {
                $player_matchgoals = $matchgoals->where($field.'_id', $this['id']);
                foreach ($player_matchgoals as $player_matchgoal) {
                    $results[$field]++;
                }
            }
        }
        $default_stats = Stats::all()
            ->where('player_id', $this['id'])
            ->where('team_id', $team['id'])
            ->first();
        if ($default_stats != null) {
            $results['throw'] += $default_stats['throw'];
            $results['asist1'] += $default_stats['asist'];
        }
        return [
            "throw" => $results['throw'],
            "asist" => $results['asist1']+$results['asist2'],
            "pass" => $results['throw']+$results['asist1']+$results['asist2'],
        ];
    }

    public function getLink() {
        return route('player.show', $this['id']);
    }

    public function getFio() {
        return $this['surname'].' '.$this['name'].' '.$this['father_name'];
    }

    public function getNameSurname() {
        return $this['name'].' '.$this['surname'];
    }

    public function getBirthday() {
        $date = Carbon::parse($this['birthday']);
        return $date->format("d.m.Y");
    }

    public function getMonthDayBirthday() {
        $date = Date::parse($this['birthday']);
        return $date->format("d F");
    }

    public function getYearsInBirthday() {
        $date = Date::parse($this['birthday']);
        $years = $date->diffInYears(Carbon::now());
        //if ($date->dayOfYear >= Date::now()->dayOfYear)
            $years += 1;
        return $years;
    }

    public function getPositionName() {
        switch ($this['position']) {
            case 1:
                return "Вратарь";
                break;
            case 2:
                return "Защитник";
                break;
            case 3:
                return "Нападающий";
                break;
            default:
                return "";
        }
    }

    public function getImage() {
        if ($this['photo_url'] != '') {
            return $this['photo_url'];
        }
        return '/img/main/blank01.jpg';
    }
}
