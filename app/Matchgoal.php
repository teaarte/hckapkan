<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matchgoal extends Model
{
    protected $fillable = [
        'match_id',
        'matchteam_id',
        'minute',
        'throw_id',
        'asist1_id',
        'asist2_id',
        'throw',
        'asist1',
        'asist2',
    ];

    public function getMatchTeam() {
        return $this->belongsTo('App\Matchteam');
    }
}
