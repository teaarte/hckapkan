<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Photoalbum extends Model
{
    protected $fillable = [
        'name',
        'created_at',
    ];

    public function photos() {
        return $this->hasMany('App\Photo');
    }


    public function getLink() {
        return route('photoalbum.show', $this['id']);
    }

    public function getThumbail() {
        $photos = $this->photos()->get();
        foreach ($photos as $photo) {
            return $photo['url'];
        }
        return '';
    }

    public function getPhotosCount() {
        $photos = $this->photos()->get();
        if ($photos != null) {
            return $photos->count();
        }
        return 0;
    }

    public function getDate() {
        $date = Carbon::parse($this['created_at']);
        return $date->format("d.m.Y");
    }

}
