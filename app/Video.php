<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class Video extends Model
{
    protected $fillable = [
        'name',
        'youtube_id',
        'created_at'
    ];


    public function getDate() {
        $date = Carbon::parse($this['created_at']);
        return $date->format("d.m.Y");
    }

    public function getWordDate() {
        Date::setLocale('ru');
        return Date::parse($this['created_at'])->format('d M Y');
    }

    public function getThumbail() {
        return "http://img.youtube.com/vi/".$this['youtube_id']."/maxresdefault.jpg";
    }

    public function getLink() {
        return route('video.show', $this['id']);
        //return "https://www.youtube.com/watch?v=".$this['youtube_id'];
    }

    public function getTitle() {
        return $this['name'];
    }

    public function getIframeUrl() {
        return "https://www.youtube.com/embed/".$this['youtube_id'];
    }
}
