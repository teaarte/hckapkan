<?php

namespace App\Http\Controllers\Panel;

use App\Arena;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelArenaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arenas = Arena::all()->sortByDesc('id');
        return view('panel.arena.index',[
            'title' => 'Список арен',
            'sidebar_group' => 'arena',
            'sidebar_element' => '',
            'arenas' => $arenas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.arena.create',[
            'title' => 'Добавить арену',
            'sidebar_group' => 'arena',
            'sidebar_element' => ''
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $arena = Arena::create($data);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arena = Arena::all()->where('id', $id)->first();
        return view('panel.arena.create',[
            'title' => 'Редактировать арену',
            'sidebar_group' => 'arena',
            'sidebar_element' => '',
            'arena' => $arena
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $arena = Arena::all()->where('id', $id)->first();
        $data = $request->all();
        $arena->update($data);
        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arena = Arena::all()->where('id',$id)->first();
//        foreach ($player->teams()->get() as $team) {
//            $team->players()->detach($player);
//        }
        $arena->delete();
        return redirect(route('panel.arena'));
    }
}
