<?php

namespace App\Http\Controllers\Panel;

use App\Calendar;
use App\Match;
use App\Matchgoal;
use App\Matchteam;
use App\Photoalbum;
use App\Player;
use App\Team;
use App\Arena;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelMatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($calendar_id = 0)
    {
        $matches = null;
        $title = 'Список матчей';
        if ($calendar_id == 0) {
            $matches = Match::all()->sortByDesc('id');
        } else {
            $matches = Match::all()
                ->where('calendar_id', $calendar_id)
                ->sortByDesc('id');
            $calendar = Calendar::all()->where('id', $calendar_id)->first();
            $title = 'Список матчей календаря '.$calendar['name'];
        }
        return view('panel.match.index',[
            'title' => $title,
            'sidebar_group' => 'match',
            'sidebar_element' => '',
            'matches' => $matches,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::all();
        $arenas = Arena::all();
        $calendars = Calendar::all();
        return view('panel.match.create',[
            'title' => 'Добавить матч',
            'sidebar_group' => 'match',
            'sidebar_element' => '',
            'teams' => $teams,
            'arenas' => $arenas,
            'calendars' => $calendars,
        ]);
    }


    public function create2($id)
    {
        $match = Match::all()->where('id', $id)->first();
        if ($match['finished'] != 1) return "";
        $match_team_1 = Matchteam::all()->where('id', $match['team1_id'])->first();
        $match_team_2 = Matchteam::all()->where('id', $match['team2_id'])->first();

        return view('panel.match.create2',[
            'match' => $match,
            'match_team_1' => $match_team_1,
            'match_team_2' => $match_team_2,
        ]);
    }

    public function create3($id) {
        $match = Match::where('id', $id)->first();
        if ($match['finished'] != 1) return "";
        $team1 = $match->getTeam(1);
        $team2 = $match->getTeam(2);
        return view('panel.match.create3', [
            'match' => $match,
            'team1' => $team1,
            'team2' => $team2,
        ]);
    }


    /**
     * Edit the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teams = Team::all();
        $arenas = Arena::all();
        $calendars = Calendar::all();
        $match = Match::where('id', $id)->first();

        return view('panel.match.create',[
            'title' => 'Редактировать матч',
            'sidebar_group' => 'match',
            'sidebar_element' => '',
            'teams' => $teams,
            'arenas' => $arenas,
            'calendars' => $calendars,
            'match' => $match,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $team_1 = Team::all()->where('id', $data['team1_id'])->first();
        $team_2 = Team::all()->where('id', $data['team2_id'])->first();

        $team_players = json_decode($data['team_players'], true);

        $match_team_1 = Matchteam::create(['team_id' => $team_1['id']]);
        foreach ($team_players["1"] as $team_player) {
            $player = Player::all()->where('id', $team_player)->first();
            $match_team_1->players()->attach($player, ['position' => $player['position']]);
        }

        $match_team_2 = Matchteam::create(['team_id' => $team_2['id']]);
        foreach ($team_players["2"] as $team_player) {
            $player = Player::all()->where('id', $team_player)->first();
            $match_team_2->players()->attach($player, ['position' => $player['position']]);
        }

        $match = Match::create([
            'arena_id' => $data['arena_id'],
            'calendar_id' => $data['calendar_id'],
            'team1_id' => $match_team_1['id'],
            'team2_id' => $match_team_2['id'],
            'finished' => $data['finished'],
            'date' => $data['date'],
        ]);

        return $match['id'];
    }

    public function store2(Request $request, $id)
    {
        $data = $request->all();
        $match = Match::all()->where('id', $id)->first();
        $match->update($data);
        return "ok";
    }

    public function store3(Request $request, $id) {
        $data = $request->all();
        $match = Match::all()->where('id', $id)->first();
        Matchgoal::where('match_id', '=', $id)->delete();
        for ($i = 1; $i <= 2; $i++) {
            for($j = 1; $j <= $match['team'.$i.'_goals']; $j++) {
                $goal_data = [
                    "minute" => $data[$i.'_'.$j.'_minute'],
                    "match_id" => $match['id'],
                    "matchteam_id" => $match["team".$i."_id"],
                ];
                if (!is_numeric($goal_data['minute'])) $goal_data['minute'] = 0;
                if (array_key_exists($i.'_'.$j.'_throw_id', $data)) {
                    $goal_data["throw_id"] = $data[$i.'_'.$j.'_throw_id'];
                    $goal_data["asist1_id"] = $data[$i.'_'.$j.'_asist1_id'];
                    $goal_data["asist2_id"] = $data[$i.'_'.$j.'_asist2_id'];
                } else {
                    $goal_data["throw"] = $data[$i.'_'.$j.'_throw'];
                    $goal_data["asist1"] = $data[$i.'_'.$j.'_asist1'];
                    $goal_data["asist2"] = $data[$i.'_'.$j.'_asist2'];
                }
                $matchgoal = Matchgoal::create($goal_data);
            }
        }
        /*
        $match->update([
            'finished' => 1,
            'team1_goals' => $data["1"]["count"],
            'team2_goals' => $data["2"]["count"],
        ]);*/
        return "ok";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Finish match.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function finish($id)
    {
        $match = Match::all()->where('id', $id)->first();
        $match_team_1 = Matchteam::all()->where('id', $match['team1_id'])->first();
        $match_team_2 = Matchteam::all()->where('id', $match['team2_id'])->first();

        return view('panel.match.finish',[
            'title' => 'Внести результаты матча',
            'sidebar_group' => 'match',
            'sidebar_element' => '',
            'match' => $match,
            'match_team_1' => $match_team_1,
            'match_team_2' => $match_team_2,
        ]);
    }

    /**
     * Finish match.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function finish_end(Request $request, $id)
    {
        $data = json_decode($request->all()["data"],true);
        $match = Match::all()->where('id', $id)->first();
        Matchgoal::where('match_id', '=', $id)->delete();
        for ($i = 1; $i <= 2; $i++) {
            foreach ($data["$i"]["goals"] as $goal) {
                $goal_data = [
                    "minute" => $goal['min'],
                    "match_id" => $match['id'],
                    "matchteam_id" => $match["team".$i."_id"],
                ];
                if (!is_numeric($goal_data['minute'])) $goal_data['minute'] = 0;
                if (array_key_exists('throwid', $goal)) {
                    $goal_data["throw_id"] = $goal['throwid'];
                    $goal_data["asist1_id"] = $goal['asistid1'];
                    $goal_data["asist2_id"] = $goal['asistid2'];
                } else {
                    $goal_data["throw"] = $goal['throw'];
                    $goal_data["asist1"] = $goal['asist1'];
                    $goal_data["asist2"] = $goal['asist2'];
                }
                $matchgoal = Matchgoal::create($goal_data);
            }
        }
        $match->update([
            'finished' => 1,
            'team1_goals' => $data["1"]["count"],
            'team2_goals' => $data["2"]["count"],
        ]);

        return redirect(route('panel.match.photo',$id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $match = Match::all()->where('id', $id)->first();
        /*$goals = Matchgoal::all()->where('match_id', $id);
        foreach ($goals as $goal) {
            $goal->delete();
        }
        $match->delete();*/
        $data = $request->all();
        //print_r($data);

        $team_players = json_decode($data['team_players'], true);

        $match_team_1 = null;
        if ($data['team1_id'] == $match->getTeam(1)['id']) {
            $match_team_1 = $match->getMatchteam(1);
            $match_team_1->players()->sync([]);
        } else {
            $match_team_1 = $match->getMatchteam(1);
            $match_team_1->delete();
            $match_team_1 = Matchteam::create(['team_id' => $data['team1_id']]);
        }

        foreach ($team_players["1"] as $team_player) {
            $player = Player::all()->where('id', $team_player)->first();
            $match_team_1->players()->attach($player, ['position' => $player['position']]);
        }



        $match_team_2 = null;
        if ($data['team2_id'] == $match->getTeam(2)['id']) {
            $match_team_2 = $match->getMatchteam(2);
            $match_team_2->players()->sync([]);
        } else {
            $match_team_2 = $match->getMatchteam(2);
            $match_team_2->delete();
            $match_team_2 = Matchteam::create(['team_id' => $data['team2_id']]);
        }
        foreach ($team_players["2"] as $team_player) {
            $player = Player::all()->where('id', $team_player)->first();
            $match_team_2->players()->attach($player, ['position' => $player['position']]);
        }

        $match->update([
            'arena_id' => $data['arena_id'],
            'calendar_id' => $data['calendar_id'],
            'team1_id' => $match_team_1['id'],
            'team2_id' => $match_team_2['id'],
            'date' => $data['date'],
            'finished' => $data['finished'],
        ]);




        return $match['id'];//$this->store($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $match = Match::all()->where('id', $id)->first();
        foreach ($match->getMatchgoals()->get() as $goal) {
            $goal->delete();
        }
        $match->delete();
        return redirect(route('panel.match'));
    }

    public function photo($id) {
        $albums = Photoalbum::all();
        $videos = Video::all();
        $match = Match::all()->where('id', $id)->first();
        return view('panel.match.photo',[
            'title' => 'Выбор фото и видео матча',
            'sidebar_group' => 'match',
            'sidebar_element' => '',
            'albums' => $albums,
            'videos' => $videos,
            'match' => $match,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function photo_store(Request $request, $id)
    {
        $match = Match::all()->where('id', $id)->first();
        $data = $request->all();
        $match->update([
            'photo_id' => $data['photo_id'],
            'video_id' => $data['video_id'],
        ]);
        return redirect(route('panel.match'));
    }
}
