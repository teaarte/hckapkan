<?php

namespace App\Http\Controllers\Panel;

use App\Photo;
use App\Photoalbum;
use Faker\Provider\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Symfony\Component\Console\Input\Input;

class PanelPhotoalbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photoalbums = Photoalbum::all();
        return view('panel.photoalbum.index',[
            'title' => 'Фотоальбомы',
            'sidebar_group' => 'photoalbum',
            'sidebar_element' => '',
            'photoalbums' => $photoalbums,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.photoalbum.create',[
            'title' => 'Создать фотоальбом',
            'sidebar_group' => 'photoalbum',
            'sidebar_element' => '',
            'route' => route('panel.photoalbum.store'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $photoalbum = Photoalbum::create($data);
        return redirect(route('panel.photoalbum.show', $photoalbum['id']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $photoalbum = Photoalbum::where('id',$id)->first();
        $photos = $photoalbum->photos()->get();
        return view('panel.photoalbum.show',[
            'title' => 'Фотоальбом',
            'sidebar_group' => 'photoalbum',
            'sidebar_element' => '',
            'photoalbum' => $photoalbum,
            'photos' => $photos,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $photoalbum = Photoalbum::all()->where('id',$id)->first();
        return view('panel.photoalbum.create',[
            'title' => 'Редактировать альбом',
            'sidebar_group' => 'photoalbum',
            'sidebar_element' => '',
            'photoalbum' => $photoalbum,
            'route' => route('panel.photoalbum.update', $id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $photoalbum = Photoalbum::all()->where('id',$id)->first();
        $photoalbum->update($data);
        return $this->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $data = $request->all();
        $album_id = 0;
        if (array_key_exists('album_id', $data)) {
            $album_id = $data['album_id'];
        }
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = $album_id.'__'.date('His').str_random(5).$filename;
            $destinationPath = base_path() . '/public/images';
            $file->move($destinationPath, $picture);
            $photo = Photo::create([
                'url' => '/images/'.$picture,
                'photoalbum_id' => $album_id,
            ]);
            $location = route('app.main').'/images/'.$picture;
            //print_r($this->resize_image_max($location, 300, 200));
            return json_encode([
                'location' => route('app.main').'/images/'.$picture,
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photoalbum = Photoalbum::all()->where('id',$id)->first();
        $photoalbum->delete();
        return redirect(route('panel.photoalbum'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_photo($id)
    {
        $photo = Photo::all()->where('id', $id)->first();
        unlink(base_path() . '/public'.$photo['url']);
        $album_id = $photo['photoalbum_id'];
        $photo->delete();
        return redirect(route('panel.photoalbum.show', $album_id));
    }
}
