<?php

namespace App\Http\Controllers\Panel;

use App\Player;
use App\Stats;
use App\Team;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelPlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $players = Player::all()->sortByDesc('id');
        return view('panel.player.index',[
            'title' => 'Список игроков',
            'sidebar_group' => 'player',
            'sidebar_element' => '',
            'players' => $players,
        ]);
    }

    protected $fields1 = [
        ['title' => 'Имя', 'name'=> 'name'],
        ['title' => 'Фамилия', 'name'=> 'surname'],
        ['title' => 'Отчество', 'name'=> 'father_name'],
        ['title' => 'Рост', 'name'=> 'height'],
        ['title' => 'Вес', 'name'=> 'weight'],
        ['title' => 'Игровой номер', 'name'=> 'number'],
    ];
    protected $fields2 = [
        ['title' => 'URL vk.com', 'name'=> 'url_vk'],
        ['title' => 'URL facebook.com', 'name'=> 'url_fb'],
        ['title' => 'URL ok.ru', 'name'=> 'url_ok'],
        ['title' => 'URL spbhl.ru', 'name'=> 'url_spbhl'],
    ];

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::all()->where('enemy', 0);
        return view('panel.player.create',[
            'title' => 'Добавить игрока',
            'sidebar_group' => 'player',
            'sidebar_element' => '',
            'fields_1' => $this->fields1,
            'fields_2' => $this->fields2,
            'teams' => $teams,
            'player_teams' => Collection::make([]),
            'route' => route('panel.player.store'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (array_key_exists('in_team',$data) == true) {
            $data['in_team'] = true;
        } else {
            $data['in_team'] = false;
        }
        if ($request->hasFile('player_photo')) {
            $file = $request->file('player_photo');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = '__'.date('His').str_random(5).$filename;
            $destinationPath = base_path() . '/public/images';
            $file->move($destinationPath, $picture);
            $data['photo_url'] = '/images/'.$picture;
        }

        $player = Player::create($data);
        $teams = Team::all()->where('enemy', 0);
        foreach ($teams as $team) {
            $newPlayers = $team->players()->get();
            if (array_key_exists('team-'.$team['id'], $data)) {
                $newPlayers->add($player);
                Stats::create([
                    'team_id' => $team['id'],
                    'player_id' => $player['id'],
                    'throw' => $data['throw-'.$team['id']],
                    'asist' => $data['asist-'.$team['id']],
                    'pass' => 0,
                ]);
            } else {
                $newPlayers = $newPlayers->diff([$player]);
            }
            $team->players()->sync($newPlayers);
        }
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teams = Team::all()->where('enemy', 0);
        $player = Player::all()->where('id',$id)->first();
        $stats = Stats::all()->where('player_id', $player['id']);
        $default_stats = [];
        foreach ($stats as $stat) {
            $default_stats[$stat['team_id']] = [
                'throw' => $stat['throw'],
                'asist' => $stat['asist'],
                'pass' => $stat['pass'],
            ];
        }
        return view('panel.player.create',[
            'title' => 'Редактировать игрока',
            'sidebar_group' => 'player',
            'sidebar_element' => '',
            'fields_1' => $this->fields1,
            'fields_2' => $this->fields2,
            'teams' => $teams,
            'player' => $player,
            'player_teams' => $player->teams()->get(),
            'route' => route('panel.player.update',$player['id']),
            'default_stats' => $default_stats,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (array_key_exists('in_team',$data) == true) {
            $data['in_team'] = true;
        } else {
            $data['in_team'] = false;
        }
        $player = Player::all()->where('id',$id)->first();
        if ($request->hasFile('player_photo')) {
            $file = $request->file('player_photo');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = $player['id'].'__'.date('His').str_random(5).$filename;
            $destinationPath = base_path() . '/public/images';
            $file->move($destinationPath, $picture);
            $data['photo_url'] = '/images/'.$picture;
        }

        $player->update($data);
        $teams = Team::all()->where('enemy', 0);
        $stats = Stats::all()->where('player_id', $player['id']);
        foreach ($stats as $stat) {
            $stat->delete();
        }
        foreach ($teams as $team) {
            $newPlayers = $team->players()->get();
            if (array_key_exists('team-'.$team['id'], $data)) {
                $newPlayers->add($player);
                Stats::create([
                    'team_id' => $team['id'],
                    'player_id' => $player['id'],
                    'throw' => $data['throw-'.$team['id']],
                    'asist' => $data['asist-'.$team['id']],
                    'pass' => 0,
                ]);
            } else {
                $newPlayers = $newPlayers->diff([$player]);
            }
            $team->players()->sync($newPlayers);
        }

        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $player = Player::all()->where('id',$id)->first();
        foreach ($player->teams()->get() as $team) {
            $team->players()->detach($player);
        }
        $player->delete();
        return redirect(route('panel.player'));
    }
}
