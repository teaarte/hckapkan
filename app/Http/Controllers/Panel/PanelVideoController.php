<?php

namespace App\Http\Controllers\Panel;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::all();
        return view('panel.video.index',[
            'title' => 'Видео',
            'sidebar_group' => 'video',
            'sidebar_element' => '',
            'videos' => $videos,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.video.create',[
            'title' => 'Добавить видео',
            'sidebar_group' => 'video',
            'sidebar_element' => '',
            'route' => route('panel.video.store'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $video = Video::create($data);
        return redirect(route('panel.video'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::all()->where('id',$id)->first();
        return view('panel.video.create',[
            'title' => 'Редактировать видео',
            'sidebar_group' => 'video',
            'sidebar_element' => '',
            'video' => $video,
            'route' => route('panel.video.update', $id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $video = Video::all()->where('id',$id)->first();
        $video->update($data);
        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::all()->where('id',$id)->first();
        $video->delete();
        return redirect(route('panel.video'));
    }
}
