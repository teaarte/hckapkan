<?php

namespace App\Http\Controllers\Panel;

use App\Console\Commands\TableParser;
use App\Table;
use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tables = Table::all()->sortByDesc('position');
        return view('panel.table.index',[
            'title' => 'Список таблиц',
            'sidebar_group' => 'table',
            'sidebar_element' => '',
            'tables' => $tables,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::all()->where('enemy', 0);
        return view('panel.table.create',[
            'title' => 'Добавить таблицу',
            'sidebar_group' => 'table',
            'sidebar_element' => '',
            'teams' => $teams,
            'route' => route('panel.table.store'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (array_key_exists('active',$data)) {
            $data['active'] = true;
        } else {
            $data['active'] = false;
        }
        if (array_key_exists('show_on_main',$data)) {
            $data['show_on_main'] = true;
        } else {
            $data['show_on_main'] = false;
        }
        $last_table = Table::all()->sortByDesc('position')->first();
        $data['position'] = $last_table['position']+1;
        $table = Table::create($data);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $table = Table::where('id', $id)->first();
        $teams = Team::all()->where('enemy', 0);
        return view('panel.table.create',[
            'title' => 'Редактировать таблицу',
            'sidebar_group' => 'table',
            'sidebar_element' => '',
            'teams' => $teams,
            'table' => $table,
            'route' => route('panel.table.update', $id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (array_key_exists('active',$data)) {
            $data['active'] = true;
        } else {
            $data['active'] = false;
        }
        if (array_key_exists('show_on_main',$data)) {
            $data['show_on_main'] = true;
        } else {
            $data['show_on_main'] = false;
        }
        $table = Table::where('id', $id)->first();
        $table->update($data);
        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $table = Table::all()->where('id',$id)->first();
        $table->delete();
        return redirect(route('panel.table'));
    }


    public function up($id)
    {
        $current_table = Table::all()->where('id', $id)->first();
        $upper_table = Table::all()
            ->where('position', '>', $current_table['position'])
            ->sortBy('position')
            ->first();
        //print_r($upper_calendar['position']);
        if ($upper_table == null) {
            print_r('no');
        } else {
            $tmp_position = $current_table['position'];
            $current_table->update(['position' => $upper_table['position']]);
            $upper_table->update(['position' => $tmp_position]);
            $current_table->save();
            $upper_table->save();
        }
        return redirect(route('panel.table'));
    }

    public function down($id)
    {
        $current_table = Table::all()->where('id', $id)->first();
        $lower_table = Table::all()
            ->where('position', '<', $current_table['position'])
            ->sortByDesc('position')
            ->first();
        print_r($lower_table['position']);

        if ($lower_table == null) {
            print_r('no');
        } else {
            $tmp_position = $current_table['position'];
            $current_table->update(['position' => $lower_table['position']]);
            $lower_table->update(['position' => $tmp_position]);
            $current_table->save();
            $lower_table->save();
        }
        return redirect(route('panel.table'));
    }

    public function update_data($id) {
        $table = Table::where('id', $id)->first();
        $parser = new TableParser();
        $parser->parse($table);
        return "Таблица успешно обновлена";
    }

}
