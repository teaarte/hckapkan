<?php

namespace App\Http\Controllers\Panel;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all()->sortByDesc('created_at');
        return view('panel.page.index',[
            'title' => 'Страницы',
            'sidebar_group' => 'page',
            'sidebar_element' => '',
            'pages' => $pages,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.page.create',[
            'title' => 'Добавить страницу',
            'sidebar_group' => 'page',
            'sidebar_element' => '',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (array_key_exists('visible_menu',$data) == true) {
            $data['visible_menu'] = true;
        } else {
            $data['visible_menu'] = false;
        }
        $news = Page::create($data);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::all()->where('id',$id)->first();
        return view('panel.page.create',[
            'title' => 'Редактировать страницу',
            'sidebar_group' => 'page',
            'sidebar_element' => '',
            'page' => $page,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (array_key_exists('visible_menu',$data) == true) {
            $data['visible_menu'] = true;
        } else {
            $data['visible_menu'] = false;
        }
        $page = Page::all()->where('id',$id)->first();
        $page->update($data);
        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::all()->where('id',$id)->first();
        $page->delete();
        return redirect(route('panel.page'));
    }
}
