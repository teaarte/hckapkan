<?php

namespace App\Http\Controllers\Panel;

use App\Calendar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelCalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calendars = Calendar::all()->sortByDesc('position');
        return view('panel.calendar.index',[
            'title' => 'Список календарей',
            'sidebar_group' => 'calendar',
            'sidebar_element' => '',
            'calendars' => $calendars,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.calendar.create',[
            'title' => 'Добавить календарь',
            'sidebar_group' => 'calendar',
            'sidebar_element' => '',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $last_calendar = Calendar::all()->sortByDesc('position')->first();
        $data['position'] = $last_calendar['position']+1;
        $calendar = Calendar::create($data);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function up($id)
    {
        $current_calendar = Calendar::all()->where('id', $id)->first();
        $upper_calendar = Calendar::all()
            ->where('position', '>', $current_calendar['position'])
            ->sortBy('position')
            ->first();
        //print_r($upper_calendar['position']);
        if ($upper_calendar == null) {
            print_r('no');
        } else {
            $tmp_position = $current_calendar['position'];
            $current_calendar->update(['position' => $upper_calendar['position']]);
            $upper_calendar->update(['position' => $tmp_position]);
            $current_calendar->save();
            $upper_calendar->save();
        }
        return redirect(route('panel.calendar'));
    }

    public function down($id)
    {
        $current_calendar = Calendar::all()->where('id', $id)->first();
        $lower_calendar = Calendar::all()
            ->where('position', '<', $current_calendar['position'])
            ->sortByDesc('position')
            ->first();
        print_r($lower_calendar['position']);

        if ($lower_calendar == null) {
            print_r('no');
        } else {
            $tmp_position = $current_calendar['position'];
            $current_calendar->update(['position' => $lower_calendar['position']]);
            $lower_calendar->update(['position' => $tmp_position]);
            $current_calendar->save();
            $lower_calendar->save();
        }
        return redirect(route('panel.calendar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $calendar = Calendar::all()->where('id',$id)->first();
        return view('panel.calendar.create',[
            'title' => 'Добавить календарь',
            'sidebar_group' => 'calendar',
            'sidebar_element' => '',
            'calendar' => $calendar,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $calendar = Calendar::all()->where('id',$id)->first();
        $calendar->update(['name' => $data['name']]);
        $calendar->save();
        return redirect(route('panel.calendar'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $calendar = Calendar::all()->where('id', $id)->first();
        $calendar->delete();
        return redirect(route('panel.calendar'));
    }
}
