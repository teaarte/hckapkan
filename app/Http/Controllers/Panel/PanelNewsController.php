<?php

namespace App\Http\Controllers\Panel;

use App\News;
use App\Photoalbum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all()->sortByDesc('created_at');
        return view('panel.news.index',[
            'title' => 'Новости',
            'sidebar_group' => 'news',
            'sidebar_element' => '',
            'news' => $news,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $albums = Photoalbum::all();
        return view('panel.news.create',[
            'title' => 'Добавить новость',
            'sidebar_group' => 'news',
            'sidebar_element' => '',
            'albums' => $albums,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (array_key_exists('draft',$data) == true) {
            $data['draft'] = true;
        } else {
            $data['draft'] = false;
        }
        $news = News::create($data);
        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::all()->where('id',$id)->first();
        $albums = Photoalbum::all();
        return view('panel.news.create',[
            'title' => 'Редактировать новость',
            'sidebar_group' => 'news',
            'sidebar_element' => '',
            'news' => $news,
            'albums' => $albums,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (array_key_exists('draft',$data) == true) {
            $data['draft'] = true;
        } else {
            $data['draft'] = false;
        }
        $news = News::all()->where('id',$id)->first();
        $news->update($data);
        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::all()->where('id',$id)->first();
        $news->delete();
        return redirect(route('panel.news'));
    }
}
