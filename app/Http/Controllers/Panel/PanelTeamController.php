<?php

namespace App\Http\Controllers\Panel;

use App\Arena;
use App\Team;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexOur()
    {
        $teams = Team::all()->where('enemy',0)->sortByDesc('id');
        return view('panel.team.index',[
            'title' => 'Список наших команд',
            'sidebar_group' => 'team',
            'sidebar_element' => 'our',
            'teams' => $teams,
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexEnemy()
    {
        $teams = Team::all()->where('enemy',1)->sortByDesc('id');
        return view('panel.team.index',[
            'title' => 'Список команд соперников',
            'sidebar_group' => 'team',
            'sidebar_element' => 'enemy',
            'teams' => $teams,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arenas = Arena::all();
        return view('panel.team.create',[
            'title' => 'Добавить команду',
            'sidebar_group' => 'team',
            'sidebar_element' => '',
            'arenas' => $arenas,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('logotype_photo')) {
            $file = $request->file('logotype_photo');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = '__'.date('His').str_random(5).$filename;
            $destinationPath = base_path() . '/public/images';
            $file->move($destinationPath, $picture);
            $data['logotype_url'] = '/images/'.$picture;
        }
        $team = Team::create($data);
        if ($team['enemy'] == 1) {
            return redirect(route('panel.team.enemy'));
        } else {
            return redirect(route('panel.team.our'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::all()->where('id', $id)->first();
        $players = null;
        if ($team['enemy'] == 0 && $team != null) {
            $players = $team->players()->get();
        }
        $arenas = Arena::all();
        return view('panel.team.create',[
            'title' => 'Редактировать команду',
            'sidebar_group' => 'team',
            'sidebar_element' => '',
            'team' => $team,
            'players' => $players,
            'arenas' => $arenas,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $team = Team::all()->where('id',$id)->first();
        if ($request->hasFile('logotype_photo')) {
            $file = $request->file('logotype_photo');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture = '__'.date('His').str_random(5).$filename;
            $destinationPath = base_path() . '/public/images';
            $file->move($destinationPath, $picture);
            $data['logotype_url'] = '/images/'.$picture;
        }
        $team->update($data);
        $team->save();
        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::all()->where('id', $id)->first();
        $enemy = false;
        if ($team['enemy'] == true) { $enemy = true; }
        $team->delete();
        if ($enemy) {
            return redirect(route('panel.team.enemy'));
        } else {
            return redirect(route('panel.team.our'));
        }
    }


    /**
     * Show the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function apiShow($id)
    {
        ///DebugBar::
        $team = Team::all()->where('id', $id)->first();
        $players = null;
        if ($team['enemy'] == 0 && $team != null) {
            $players = $team->players()->get();
            print_r(json_encode($players));
        } else {
            print_r(json_encode([]));
        }
        exit();
    }
}
