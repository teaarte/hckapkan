<?php

namespace App\Http\Controllers;

use App\Player;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class PlayerController extends Controller
{

    public function index() {
        return redirect(route('app.main'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $player = Player::all()->where('id', $id)->first();
        $birthday = Carbon::parse($player['birthday']);
        $years = null;
        if ($birthday != null) {
            $years = $birthday->diffInYears(Carbon::now());
        }
        $stats = $player->getStats();
        return view('pages.player.show', [
            'title' => config('app.name', 'ХК «Капкан»').' - '.$player->getFio(),
            'player' => $player,
            'years' => $years,
            'birthday' => $birthday,
            'stats' => $stats,
        ]);
    }

}