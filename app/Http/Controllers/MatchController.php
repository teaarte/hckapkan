<?php

namespace App\Http\Controllers;

use App\Match;
use App\Player;
use Illuminate\Http\Request;

class MatchController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $match = Match::all()->where('id', $id)->first();

        $matchgoals = $match->getMatchgoals()->get()->sortBy('minute');
        $period_results = [
            [0 , 0],
            [0 , 0],
            [0 , 0]
        ];
        $goals_results = [
            [],
            [],
            []
        ];
        $common_results = [0, 0];
        foreach ($matchgoals as $matchgoal) {
            $team = 0;
            if ($matchgoal['matchteam_id'] == $match['team2_id']) {
                $team = 1;
            }
            $period = 0;
            if ($matchgoal['minute'] <= 20) $period = 0;
            else if ($matchgoal['minute'] <= 40) $period = 1;
            else if ($matchgoal['minute'] <= 60) $period = 2;
            $period_results[$period][$team]++;

            $throw = '';
            $asist1 = '';
            $asist2 = '';

            $throw_link = '';
            $asist1_link = '';
            $asist2_link = '';

            if ($matchgoal['throw_id'] != 0) {
                $throw_player = Player::all()->where('id', $matchgoal['throw_id'])->first();
                if ($throw_player != null) {
                    $throw = $throw_player->getNameSurname();
                    $throw_link = route('player.show', $throw_player['id']);
                }
                $asist1_player = Player::all()->where('id', $matchgoal['asist1_id'])->first();
                if ($asist1_player != null) {
                    $asist1 = $asist1_player->getNameSurname();
                    $asist1_link = route('player.show', $asist1_player['id']);
                }
                $asist2_player = Player::all()->where('id', $matchgoal['asist2_id'])->first();
                if ($asist2_player != null) {
                    $asist2 = $asist1_player->getNameSurname();
                    $asist2_link = route('player.show', $asist2_player['id']);
                }
            } else {
                $throw = $matchgoal['throw'];
                $asist1 = $matchgoal['asist1'];
                $asist2 = $matchgoal['asist2'];
            }
            $common_results[$team] ++;

            $goal = [
                'team' => $team,
                'throw' => $throw,
                'asist1' => $asist1,
                'asist2' => $asist2,
                'throw_link' => $throw_link,
                'asist1_link' => $asist1_link,
                'asist2_link' => $asist2_link,
                'common_results' => $common_results,
                'time' => $matchgoal['minute'].':00',
            ];
            array_push($goals_results[$period], $goal);
        }

        $matchteam = null;
        if ($match->getTeam(1)['enemy'] == 0) {
            $matchteam = $match->getMatchteam(1);
        } else {
            $matchteam = $match->getMatchteam(2);
        }
        $position_names = [
            ['position' => 1, 'name' => 'Вратари'],
            ['position' => 2, 'name' => 'Защитники'],
            ['position' => 3, 'name' => 'Нападающие'],
        ];

        $photo = $match->getPhotoalbum();
        $video = $match->getVideo();

        return view('pages.match.show', [
            'title' => config('app.name', 'ХК «Капкан»').' - Матч',
            'match' => $match,
            'period_results' => $period_results,
            'goals_results' => $goals_results,
            'matchteam' => $matchteam,
            'position_names' => $position_names,
            'photo' => $photo,
            'video' => $video,
        ]);
    }

}
