<?php

namespace App\Http\Controllers;

use App\Calendar;
use App\Match;
use App\Matchgoal;
use App\News;
use App\Photoalbum;
use App\Player;
use App\Table;
use App\Team;
use App\Video;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class MainController extends Controller
{
    protected $data = null;
    /**
     * Show main page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data = Cache::remember('main_page_data', 5, function () {
            return [
                'title' => config('app.name', 'ХК «Капкан»').' - Главная',
                'last_news' => $this->getLastNews(),
                'last_matches' => $this->getMatches(),
                'feature_matches' => $this->getMatches(true),
                'best_players' => $this->getBestPlayes(),
                'tables' => $this->getTables(),
                'media_column' => $this->getMediaColumn(),
                'birthday_column' => $this->getBirthdayColumn(),
            ];
        });
        $view = Cache::remember('main_page_view', 5, function () {
            return view('main', $this->data)->render();
        });
        return $view;
    }

    protected function getLastNews() {
        $news = News::all()->sortByDesc('created_at')->slice(0,4);
        return $news;
    }

    protected function getMatches($feature = false) {
        $teams = Team::all()->where('enemy', 0);
        $team_matches = [];

        $matches = null;
        if ($feature) {
            $matches = Match::all()->sortBy('date')->slice(0, 3)
                ->where('finished', 0);
        } else {
            $matches = Match::all()->sortByDesc('date')->slice(0, 3)
                ->where('finished', 1);
        }

        foreach ($matches as $match) {

        }
        array_push($team_matches, [
            'team_id' => 0,
            'team' => 'Все команды',
            'matches' => $matches->unique(),
        ]);

        foreach ($teams as $team) {
            $matchteams = $team->getMatchteams()->get();
            $matches = Collection::make();
            foreach ($matchteams as $matchteam) {
                if ($feature) {
                    $_matches = Match::all()
                        ->where('finished', 0)
                        ->where('team1_id', $matchteam['id']);
                    foreach ($_matches as $match) {
                        $matches->add($match);
                    }
                    $_matches = Match::all()
                        ->where('finished', 0)
                        ->where('team2_id', $matchteam['id']);
                    foreach ($_matches as $match) {
                        $matches->add($match);
                    }
                } else {
                    $_matches = Match::all()
                        ->where('finished', 1)
                        ->where('team1_id', $matchteam['id']);
                    foreach ($_matches as $match) {
                        $matches->add($match);
                    }
                    $_matches = Match::all()
                        ->where('finished', 1)
                        ->where('team2_id', $matchteam['id']);
                    foreach ($_matches as $match) {
                        $matches->add($match);
                    }
                }
                $matches = $matches->sortByDesc('date')->slice(0, 3);
            }
            array_push($team_matches, [
                'team' => $team['name'],
                'team_id' => $team['id'],
                'matches' => $matches->unique(),
            ]);
        }
        //print_r($team_matches);
        return [
            'teams' => $team_matches,
        ];
    }

    protected function getBestPlayes() {
        $calendars = Calendar::all();
        $teams = Team::all()->where('enemy', 0);
        $items = [
            [
                'name' => "Снайпер",
                'slug' => 'sniper',
                'currency' => "голов"
            ],
            [
                'name' => "Бомбардир",
                'slug' => 'bomber',
                'currency' => "очков"
            ],
            [
                'name' => "Ассистент",
                'slug' => 'asist',
                'currency' => "передач"
            ],
        ];
        return [
            'calendars' => $calendars,
            'teams' => $teams,
            'items' => $items,
        ];
    }

    protected function getTables() {
        $tables = Table::all()
            ->where('show_on_main', true)
            ->sortByDesc('position');
        return [
            'tables' => $tables,
        ];
    }

    protected function getMediaColumn() {
        $photos_all = Photoalbum::all()->sortByDesc('created_at');
        $photos = Collection::make();
        foreach ($photos_all as $photo) {
            if ($photo->getPhotosCount()>0) {
                $photos->add($photo);
            }
        }
        $videos = Video::all()->sortByDesc('created_at')->slice(0,2);
        return [
            'photos' => $photos,
            'videos' => $videos,
            'photo_link' => route('photoalbum.index'),
            'video_link' => route('video.index'),
        ];
    }

    protected function getBirthdayColumn() {
        $players = Player::whereRaw('DAYOFYEAR(curdate())+1 < DAYOFYEAR(birthday)')
            ->orderByRaw('DAYOFYEAR(birthday)')
            ->get()
            ->slice(0, 3);
        $players2 = Player::whereRaw('0 < DAYOFYEAR(curdate())+1')
            ->orderByRaw('DAYOFYEAR(birthday)')
            ->get()
            ->slice(0, 3);
        foreach ($players2 as $player) {
            $players->add($player);
        }
        $players = $players->slice(0, 3)->unique();
        //print_r($players);
        return ['players' => $players];
    }

    public function bestplayers(Request $request) {
        $data = $request->all();
        //print_r($data);
        $calendars = Calendar::all();
        if ($data['calendar_id'] != 0) {
            $calendars = Calendar::all()->where('id', $data['calendar_id']);
        }
        $players = array();
        foreach ($calendars as $calendar) {
            $matches = $calendar->getMatches()->get()->where('finished', 1);
            foreach ($matches as $match) {
                $matchgoals = Matchgoal::all()->where('match_id', $match['id']);
                foreach ($matchgoals as $matchgoal) {
                    if ($matchgoal['throw_id'] != 0) {
                        if (!array_key_exists($matchgoal['throw_id'], $players)) {
                            $players[$matchgoal['throw_id']] = [
                                'throw' => 0,
                                'asist' => 0,
                                'bomb' => 0,
                            ];
                        }
                        $players[$matchgoal['throw_id']]['throw'] += 1;
                    }
                    if ($matchgoal['asist1_id'] != 0) {
                        if (!array_key_exists($matchgoal['asist1_id'], $players)) {
                            $players[$matchgoal['asist1_id']] = [
                                'throw' => 0,
                                'asist' => 0,
                                'bomb' => 0,
                            ];
                        }
                        $players[$matchgoal['asist1_id']]['asist'] += 1;
                    }
                    if ($matchgoal['asist2_id'] != 0) {
                        if (!array_key_exists($matchgoal['asist2_id'], $players)) {
                            $players[$matchgoal['asist2_id']] = [
                                'throw' => 0,
                                'asist' => 0,
                                'bomb' => 0,
                            ];
                        }
                        $players[$matchgoal['asist2_id']]['asist'] += 1;
                    }
                }
            }
        }
        $_players = array();
        foreach ($players as $player_id => $value) {
            $value['bomb'] = $value['throw']+$value['asist'];
            $_players[$player_id] = $value;
        }
        $players = $_players;


        $sniper = null;
        $bomber = null;
        $asist = null;

        //Sniper
        $sniper_players = $players;
        if ($data['sniper_team_id'] != 0) {
            $sniper_players = array();
            $team_id = $data['sniper_team_id'];
            foreach ($players as $player_id => $value) {
                $player = Player::all()->where('id', $player_id)->first();
                if ($player != null) {
                    $player_teams = $player->teams()->get();
                    foreach ($player_teams as $player_team) {
                        if ($player_team['id'] == $team_id) {
                            $sniper_players[$player_id] = $value;
                        }
                    }
                }
            }
        }

        foreach ($sniper_players as $sniper_player => $value) {
            $sniper = $sniper_player;
            break;
        }
        foreach ($sniper_players as $sniper_player => $value) {
            if ($sniper_players[$sniper]['throw'] < $value['throw']) {
                $sniper = $sniper_player;
            }
        }

        //Bomber
        $bomber_players = $players;
        if ($data['bomber_team_id'] != 0) {
            $bomber_players = array();
            $team_id = $data['bomber_team_id'];
            foreach ($players as $player_id => $value) {
                $player = Player::all()->where('id', $player_id)->first();
                if ($player != null) {
                    $player_teams = $player->teams()->get();
                    foreach ($player_teams as $player_team) {
                        if ($player_team['id'] == $team_id) {
                            $bomber_players[$player_id] = $value;
                        }
                    }
                }
            }
        }

        foreach ($bomber_players as $bomber_player => $value) {
            $bomber = $bomber_player;
            break;
        }
        foreach ($bomber_players as $bomber_player => $value) {
            if ($bomber_players[$bomber]['bomb'] < $value['bomb']) {
                $bomber = $bomber_player;
            }
        }


        //Asist
        $asist_players = $players;
        if ($data['asist_team_id'] != 0) {
            $asist_players = array();
            $team_id = $data['asist_team_id'];
            foreach ($players as $player_id => $value) {
                $player = Player::all()->where('id', $player_id)->first();
                if ($player != null) {
                    $player_teams = $player->teams()->get();
                    foreach ($player_teams as $player_team) {
                        if ($player_team['id'] == $team_id) {
                            $asist_players[$player_id] = $value;
                        }
                    }
                }
            }
        }

        foreach ($asist_players as $bomber_player => $value) {
            $asist = $bomber_player;
            break;
        }
        foreach ($asist_players as $bomber_player => $value) {
            if ($asist_players[$asist]['asist'] < $value['asist']) {
                $asist = $bomber_player;
            }
        }


        $result = [
            "sniper" => [],
            "bomber" => [],
            "asist" => [],
        ];

        if ($sniper != null) {
            $sniper_player = Player::all()->where('id', $sniper)->first();
            $result['sniper'] = [
                'name' => $sniper_player->getFio(),
                'photo' => $sniper_player->getImage(),
                'result' => $players[$sniper]['throw'],
                'id' => $sniper_player['id'],
            ];
        } else {
            $result['sniper'] = [
                'id' => '',
                'name' => '',
                'photo' => '/img/main/blank01.jpg',
                'result' => 0,
            ];
        }

        if ($bomber != null) {
            $bomber_player = Player::all()->where('id', $bomber)->first();
            $result['bomber'] = [
                'name' => $bomber_player->getFio(),
                'photo' => $bomber_player->getImage(),
                'result' => $players[$bomber]['bomb'],
                'id' => $bomber_player['id'],
            ];
        } else {
            $result['bomber'] = [
                'id' => '',
                'name' => '',
                'photo' => '/img/main/blank01.jpg',
                'result' => 0,
            ];
        }

        if ($asist != null) {
            $asist_player = Player::all()->where('id', $asist)->first();
            $result['asist'] = [
                'name' => $asist_player->getFio(),
                'photo' => $asist_player->getImage(),
                'result' => $players[$asist]['asist'],
                'id' => $asist_player['id'],
            ];
        } else {
            $result['asist'] = [
                'id' => '',
                'name' => '',
                'photo' => '/img/main/blank01.jpg',
                'result' => 0,
            ];
        }

        print_r(json_encode($result));

        exit();
    }
}
