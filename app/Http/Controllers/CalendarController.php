<?php

namespace App\Http\Controllers;

use App\Calendar;
use App\Team;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->show();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($calendar_id = 0, $team_id = 0)
    {
        $calendars = Calendar::all()->sortByDesc('position');
        $teams = Team::all()->where('enemy', 0);

        $calendar = null;
        $matches = Collection::make();
        if ($calendar_id == 0) {
            $calendar_id = 0;
            foreach ($calendars as $calendar) {
                $_matches = $calendar->getMatches()->get();
                foreach ($_matches as $match) {
                    $matches->add($match);
                }
            }
            $matches = $matches->unique()->sortBy('finished');
        } else {
            $calendar = $calendars->where('id', $calendar_id)->first();
            $matches = $calendar->getMatches()->get()->sortBy('finished');
        }

        if ($team_id != 0) {
            $tmp = Collection::make();
            foreach ($matches as $match) {
                $match_team1_id = $match->getTeam(1)['id'];
                $match_team2_id = $match->getTeam(2)['id'];
                if ($match_team1_id == $team_id || $match_team2_id == $team_id) {
                    $tmp->add($match);
                }
            }
            $matches = $tmp;
        }

        return view('pages.calendar.show', [
            'title' => config('app.name', 'ХК «Капкан»').' - Календарь',
            'calendars' => $calendars,
            'teams' => $teams,
            'calendar_id' => $calendar_id,
            'team_id' => $team_id,
            'matches' => $matches
        ]);
    }

}
