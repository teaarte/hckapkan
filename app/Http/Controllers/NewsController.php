<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $news_per_page = 8;
        $start_news = 0;
        $data = $request->all();
        $page = 1;
        if (array_key_exists('get_page', $data)) {
            $page = $data['get_page'];
            $start_news = ($page-1) * $news_per_page;
        }
        $news = News::all()->sortByDesc('created_at')->slice($start_news, $news_per_page);
        $paginator = true;
        if ($start_news+$news_per_page >= News::all()->count()) {
            $paginator = false;
        }
        if ($page != 1) {
            return view('pages.news.news', [
                'news' => $news,
                'page' => $page+1,
                'paginator' => $paginator,
            ]);
        }
        return view('pages.news.index', [
            'title' => config('app.name', 'ХК «Капкан»').' - Новости',
            'news' => $news,
            'paginator' => $paginator,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $slug)
    {
        $news = News::all()->where('id',$id)->first();
        $generated_slug = str_slug($news['title'],'-');
        if ($news != null && $slug == $generated_slug) {
            return view('pages.news.show', [
                'title' => config('app.name', 'ХК «Капкан»').' - '.$news->getTitle(),
                'news' => $news
            ]);
        } else {
            return redirect(route('app.main'));
        }
    }
}
