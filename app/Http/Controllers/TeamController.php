<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->show(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $players = Collection::make();
        $teams = Team::all()->where('enemy', 0);
        if ($id == 0) {
            foreach ($teams as $team) {
                $_players = $team->players()->get()->where('in_team', 1);
                foreach ($_players as $player) {
                    $players->add($player);
                }
            }
            //print_r($players);
        } else {
            $team = $teams->where('id', $id)->first();
            $players = $team->players()->get()->where('in_team', 1);
        }
        $position_names = [
            ['position' => 1, 'name' => 'Вратари'],
            ['position' => 2, 'name' => 'Защитники'],
            ['position' => 3, 'name' => 'Нападающие'],
        ];

        return view('pages.team.show', [
            'title' => config('app.name', 'ХК «Капкан»').' - Состав команды ',
            'teams' => $teams,
            'position_names' => $position_names,
            'players' => $players->unique(),
            'team_id' => $id,
        ]);
    }

}