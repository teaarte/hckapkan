<?php

namespace App\Http\Controllers;

use App\Photoalbum;
use App\Video;
use Illuminate\Http\Request;

class PhotoalbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photoalbums = Photoalbum::all()->sortByDesc('created_at')->slice(0, 20);
        return view('pages.photoalbum.index', [
            'title' => config('app.name', 'ХК «Капкан»').' - Фотоальбомы',
            'type' => 'photo',
            'photoalbums' => $photoalbums
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_video()
    {
        $photoalbums = Video::all()->sortByDesc('created_at')->slice(0, 20);
        return view('pages.photoalbum.index', [
            'title' => config('app.name', 'ХК «Капкан»').' - Видео',
            'type' => 'video',
            'photoalbums' => $photoalbums
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $photoalbum = Photoalbum::all()->where('id', $id)->first();
        return view('pages.photoalbum.show', [
            'title' => config('app.name', 'ХК «Капкан»').' - '.$photoalbum['name'],
            'photoalbum' => $photoalbum
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
