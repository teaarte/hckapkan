<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $fillable = [
        'name',
        'position'
    ];

    public function getMatches() {
        return $this->hasMany("App\Match");
    }
}
