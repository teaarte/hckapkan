<!-- header -->
<header class="header">
    <div class="inner-wrap">
        <a href="/" class="logo">
            <img src="../img/main/logo.png" alt="">
        </a>
        <div class="logo-text">Хоккейный клуб «Капкан»</div>

    </div>
</header>
<!-- /header -->
<!-- nav -->
<nav class="nav">
    <div class="inner-wrap">
        <div class="main-menu-wrap popup-wrap">
            <a href="" class="btn-menu btn-toggle">Меню</a>
            <div class="menu-block popup-block">
                <ul>
                    <li><a href="{{ route('news.index') }}">Новости</a></li>
                    <li><a href="{{ route('team.index') }}">Состав</a></li>
                    <li><a href="{{ route('calendar.index') }}">Календарь</a></li>
                    <li><a href="{{ route('table.index') }}">Таблицы</a>
                    </li>
                    <li>
                        <a >Медиа</a>
                        <ul>
                            <li><a href="{{ route('photoalbum.index') }}">Фотогалерея</a>
                            </li>
                            <li><a href="{{ route('video.index') }}">Видеогалерея</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>О клубе</a>
                        <ul>
                            @foreach(\App\Page::all()->where('visible_menu', true) as $page)
                            <li>
                                <a href="{{ $page->getUrl() }}">{{ $page->getTitle() }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- /nav -->