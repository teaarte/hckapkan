<!-- column -->
<div class="column">
    <h2>МЕДИА</h2>
    <div class="media-gallery-box">
        <div class="items-wrap">
            @foreach($photos as $photo)
                <!-- item -->
                <div class="item-wrap">
                    <a href="{{ $photo->getLink() }}" class="item-mgallery">
                        <span class="photo-block">
                            <img src="{{ $photo->getThumbail() }}" alt="">
                            <span class="frm-count">{{ $photo->getPhotosCount() }}</span>
                        </span>
                        <span class="title-block">{{ $photo['name'] }}</span>
                        <span class="frm-date">{{ $photo->getDate() }}</span>
                    </a>
                </div>
                <!-- /item -->
            @endforeach
            <div class="link-block">
                <a href="{{ $photo_link }}">Все фото</a>
            </div>
        </div>
        <div class="items-wrap">
            @foreach($videos as $video)
                <!-- item -->
                <div class="item-wrap">
                    <a href="{{ $video->getLink() }}" class="item-mgallery">
                        <span class="video-block">
                            <img src="{{ $video->getThumbail() }}" alt="">
                        </span>
                        <span class="title-block">{{ $video['name'] }}</span>
                        <span class="frm-date">{{ $video->getDate() }}</span>
                    </a>
                </div>
                <!-- /item -->
            @endforeach
            <div class="link-block">
                <a href="{{ $video_link }}">Все видео</a>
            </div>
        </div>
    </div>
</div>
<!-- /column -->