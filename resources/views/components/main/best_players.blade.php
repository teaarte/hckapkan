<script>
    var data = {
        "calendar_id": 0,
        @foreach($items as $item)
        "{{ $item['slug'] }}_team_id" : 0,
        @endforeach
        "_token": "{{ csrf_token() }}"
    };

    $( document ).ready(function() {
        $('#best-calendar-select').on('selectmenuchange', function (event, ui) {
            data.calendar_id = ui.item.value;
            updateData();
        });

        @foreach($items as $item)
            $('#best-team-select-{{ $item['slug'] }}').on('selectmenuchange', function (event, ui) {
                data.{{ $item['slug'] }}_team_id = ui.item.value;
                updateData();
            })
        @endforeach
        updateData();

    });

    function updateData() {
        console.log(data);
        $.post("{{ route('api.bestplayers') }}", data).done(function(response) {
            console.log(response);
            var json = JSON.parse(response);
            @foreach($items as $item)
            var block = $('#best-player-{{ $item['slug'] }}');
            block.attr('href', "{{ route('player.index') }}/"+json.{{ $item['slug'] }}.id);
            block.find('.name-block').html(json.{{ $item['slug'] }}.name);
            block.find('.photo-block').html('<img src="'+json.{{ $item['slug'] }}.photo+'" alt="">');
            block.find('.points').html(json.{{ $item['slug'] }}.result);
            @endforeach
        });


    }
</script>

<!-- best players box -->
<div class="best-players-box frm-bg">
    <div class="title-block">
        <img src="./img/title.png" alt="">
        <h2 class="main-title">Лучшие игроки</h2>
        <div class="select-block w210">
            <select id="best-calendar-select">
                <option value="0">Все турниры</option>
                @foreach($calendars as $calendar)
                    <option value="{{ $calendar['id'] }}">{{ $calendar['name'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="items-wrap">
        @foreach($items as $item)
            <!-- item -->
            <div class="item-wrap">
                <div class="item-title">
                    <div class="title">{{ $item['name'] }}</div>
                    <div class="select-block">
                        <select id="best-team-select-{{ $item['slug'] }}">
                            <option value="0">Все команды</option>
                            @foreach($teams as $team)
                                <option value="{{ $team['id'] }}">{{ $team['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <a href="" class="item-player" id="best-player-{{ $item['slug'] }}">
                    <span class="photo-block">
                        <img src="/img/main/blank01.jpg" alt=""></span>
                    <span class="name-block"></span>
                    <span class="frm-points">
                        <span class="points">0</span> {{ $item['currency'] }}
                    </span>
                </a>
            </div>
            <!-- /item -->
        @endforeach
    </div>
</div>
<!-- /best players box -->