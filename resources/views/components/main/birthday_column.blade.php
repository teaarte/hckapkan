<!-- column -->
<div class="column">
    <h2>Дни рождения</h2>
    <!-- players box -->
    <div class="players-box">
        @foreach($players as $player)
            <!-- item -->
            <div class="item-wrap">
                <a href="{{ $player->getLink() }}" class="item-player main">
                    <span class="photo-block"><img src="{{ $player->getImage() }}" alt=""></span>
                    <span class="name-block">{{ $player->getFio() }}</span>
                    <span class="date-block">{{ $player->getMonthDayBirthday() }}</span>
                    <span class="age-block">Исполнится {{ $player->getYearsInBirthday() }} лет</span>
                </a>
            </div>
            <!-- /item -->
        @endforeach
    </div>
    <!-- /players box -->
</div>
<!-- /column -->