<!-- column -->
<div class="column">
    <h3>Новости</h3>
    <!-- news box -->
    <div class="news-list-box">

        @foreach($last_news as $news)
            <!-- item -->
            <div class="item-wrap">
                <div class="item-text-new">
                    <div class="frm-date">{{ $news->getDate() }}</div>
                    <p>
                        <a href="{{ $news->getUrl() }}">{{ $news['title'] }}</a>
                    </p>
                </div>
            </div>
            <!-- /item -->
        @endforeach

        <div class="link-block"><a href="{{ route('news.index') }}">Все новости</a>
        </div>
    </div>
    <!-- /news box -->
</div>
<!-- /column -->