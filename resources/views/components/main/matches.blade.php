<?php
    $rnd = str_random(10);
?>

<script>
    $( document ).ready(function() {
        $('#{{ $rnd }}-match-team-select').on('selectmenuchange', function (event, ui) {
            var team_id = ui.item.value;
            @foreach($matches['teams'] as $team)
                $('#{{ $rnd }}-team-matches-{{$team['team_id']}}').hide();
            @endforeach
            $('#{{ $rnd }}-team-matches-'+team_id).show();
        });
        $('#{{ $rnd }}-match-team-select').trigger('selectmenuchange', [{"item": { "value": 0  } }]);
    });
</script>

<!-- column -->
<div class="column">
    <div class="title-block">
        <div class="title">
            <h3>{{ $title or "Последние матчи" }} </h3>
        </div>
        <div class="select-block">
            <select id="{{ $rnd }}-match-team-select">
                @foreach($matches['teams'] as $team)
                    <option value="{{ $team['team_id'] }}">{{ $team['team'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <!-- math box -->
    <div class="math-box">
        @foreach($matches['teams'] as $team)
            <div id="{{ $rnd }}-team-matches-{{ $team['team_id'] }}" style="display: none">
                @foreach($team['matches'] as $match)
                    <!-- item -->
                    <div class="item-wrap">
                        <a @if($match['finished'] == 1) href="{{ route('match.show', $match['id']) }}" @endif class="item-match @if($match['finished'] ==0) future @endif">
                            <span class="team-block">
                                <span class="logo">
                                    <img src="{{ $match->getTeamLogo(1) }}" alt="" style="display: block;width: 100%;">
                                </span>

                                <span class="name">{{ $match->getTeamName(1) }}</span>

                            </span>
                            <span class="result-block">

                                <span class="score">
                                    @if($match['finished'] == 1)
                                        {{ $match['team1_goals'] }}:{{ $match['team2_goals'] }}
                                    @endif
                                </span>

                                <span class="date">{{ $match->getDate() }}</span>

                            </span>
                            <span class="team-block">

                                <span class="logo">
                                    <img src="{{ $match->getTeamLogo(2) }}" alt="" style="display: block;width: 100%;">
                                </span>

                                <span class="name">{{ $match->getTeamName(2) }}</span>

                            </span>
                        </a>
                    </div>
                    <!-- /item -->
                @endforeach
            </div>
        @endforeach
        <div class="link-block"><a href="{{ route('calendar.index') }}">Все матчи</a>
        </div>
    </div>
    <!-- /math box -->
</div>
<!-- /column -->