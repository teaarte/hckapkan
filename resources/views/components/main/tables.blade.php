<!-- tables box -->
<div class="tables-box">
    <h2 class="main-title">Таблицы</h2>
    <div class="items-wrap">

        <script>
            function showTableRows(rows) {
                console.log($('.'+rows));
                $('.'+rows).show();
            }
        </script>

    @foreach($tables as $table)
        @if($table->getHeaderRow() != null)
            <!-- item -->
                <div class="item-wrap">
                    <div class="item-table">
                        <h3 class="small-title">{{ $table['name'] }}</h3>
                        <div class="tbl-tables">
                            <table class="">
                                <col class="col01">
                                <col class="col02">
                                <col class="col03">
                                <col class="col04">
                                <col class="col05">
                                <col class="col06">
                                <col class="col07">
                                <col class="col08">
                                <thead>
                                <tr>
                                    <?php
                                    $header_row = $table->getHeaderRow();
                                    ?>
                                    @for($i = 0; $i <= 8; $i++)
                                        @continue($i == 7)
                                        <th>
                                            {{ $header_row->getString($i) }}
                                        </th>
                                    @endfor
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i_row = 1;
                                $rows = $table->getBodyRows();
                                $team_name = $table->getTeamName();
                                $team_row = 0;
                                foreach ($rows as $row) {
                                    if ($team_name != "" && strpos($row->getString(1), $team_name) !== false) {
                                        $team_row = $i_row;
                                        break;
                                    }
                                    $i_row++;
                                }
                                $i_row = 1;
                                $offset = 0;
                                if ($team_row > 3 && $team_row != 0) {
                                    $offset = $team_row - 3;
                                }
                                ?>
                                <?php $showed = false; ?>
                                @foreach($rows as $row)

                                    <tr @if($i_row <= $offset) style="display:none" class="hidden-rows-{{$table['id']}}" @endif>
                                        @for($i = 0; $i <= 8; $i++)
                                            @continue($i == 7)
                                            <td>
                                                @if ($i_row == $team_row || $i == 8)
                                                    <b>{{ $row->getString($i) }}</b>
                                                @else
                                                    {{ $row->getString($i) }}
                                                @endif
                                            </td>
                                        @endfor
                                    </tr>
                                    @if(($i_row-$offset == 5 && $showed == false) ||
                                    ($showed == false && $rows->count()==$i_row)
                                    )
                                        <?php $showed = true;
                                            $_id = str_random(10);
                                        ?>
                                        <tr class="tr-link">
                                            <td colspan="8" >
                                                <a href="" id="id{{$_id}}">Смотреть полностью</a>
                                            </td>
                                        </tr>
                                        <script>
                                            $(document).ready(function(){
                                                $('#id{{$_id}}').click(function(event) {
                                                    showTableRows('hidden-rows-{{$table['id']}}')
                                                });
                                            });
                                        </script>
                                    @endif
                                    <?php $i_row++ ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /item -->
            @endif
        @endforeach
    </div>
</div>
<!-- /tables box -->