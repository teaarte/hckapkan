<!-- footer -->
<footer class="footer">
    <div class="inner-wrap">
        <a href="" class="logo">
            <img src="../img/main/logo-footer.png" alt="">
        </a>
        <div class="inner-block">
            <div class="copy-block">© 1999-2017 Хоккейный клуб «Капкан»</div>
            <div class="mail-block"><a href="mailto:info@hckapkan.ru">info@hckapkan.ru</a>
            </div>
        </div>
    </div>
</footer>
<!-- /footer -->