@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="green">
                    <i class="material-icons">add_circle_outline</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">{{ $title }}</h4>
                    <form action="{{ $route }}" method="POST"  class="form-horizontal">
                        {{ csrf_field() }}

                        <div class="row">
                            <label class="col-sm-2 label-on-left">Название</label>
                            <div class="col-sm-10">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input type="text" class="form-control" name="name" placeholder="Название" value="{{ $table['name'] or "" }}">
                                    <span class="help-block">Название</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 label-on-left">URL</label>
                            <div class="col-sm-10">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input type="text" class="form-control" name="url" placeholder="URL" value="{{ $table['url'] or "" }}">
                                    <span class="help-block">http://www.spbhl.ru/Standings?TournamentID=3789</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 label-on-left">Номер таблицы</label>
                            <div class="col-sm-10">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input type="text" class="form-control" name="table_number" placeholder="URL" value="{{ $table['table_number'] or "1" }}">
                                    <span class="help-block">Номер по порядку таблицы на странице. Например: <b>1</b> - Группа А, <b>2</b> - Группа B</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 label-on-left">Активность</label>
                            <div class="col-sm-10 checkbox-radios">
                                <div class="checkbox">
                                    <label>
                                        <?php
                                        $value = "";
                                        if (isset($table)) {
                                            if ($table['active'] == true)
                                                $value = "checked";
                                        }
                                        ?>
                                        <input type="checkbox" name="active" {{ $value }}> Парсинг па расписанию активен
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 label-on-left">Видимость</label>
                            <div class="col-sm-10 checkbox-radios">
                                <div class="checkbox">
                                    <label>
                                        <?php
                                        $value = "";
                                        if (isset($table)) {
                                            if ($table['show_on_main'] == true)
                                                $value = "checked";
                                        }
                                        ?>
                                        <input type="checkbox" name="show_on_main" {{ $value }}> Отображать на главной
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 label-on-left">Команда</label>
                            <div class="col-sm-10" style="padding-top: 6px">
                                <select class="selectpicker" data-style="select-with-transition" title="Выберите команду" data-size="7" name="team_id">
                                    <option value="0"> Выберите команду</option>
                                    @foreach($teams as $team)
                                        <option value="{{ $team['id'] }}" <?php if(isset($table)) if ($table['team_id'] == $team['id']) echo "selected "?>>{{ $team['id'] }} - {{ $team['name'] }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <br>
                        <br>

                        <div align="center">
                            <input type="submit" class="btn btn-success" value="Сохранить" style="width: 70%">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection