@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="rose">
                    <i class="material-icons">grid_on</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">{{ $title }}</h4>
                    <div class="row">
                        <div class="col-md-12" align="right">

                            <a href="{{ route('panel.table.create') }}" class="btn btn btn-success" >
                                <i class="material-icons">add_circle_outline</i> Добавить таблицу
                            </a>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tables as $table)
                                        <tr>
                                            <td>
                                                {{ $table['name'] }}
                                            </td>
                                            <td>
                                                <a href="{{ route('panel.table.update_data', $table['id']) }}" class="btn btn-sm btn-success" alt="Обновить таблицу" target="_blank" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">update</i>
                                                </a>

                                                <a href="{{ route('panel.table.edit', $table['id']) }}" class="btn btn-sm btn-info" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>

                                                <a href="{{ route('panel.table.up', $table['id']) }}" class="btn btn-sm btn-default" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">keyboard_arrow_up</i>
                                                </a>

                                                <a href="{{ route('panel.table.down', $table['id']) }}" class="btn btn-sm btn-default" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">keyboard_arrow_down</i>
                                                </a>

                                                <a href="{{ route('panel.table.destroy', $table['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">delete_forever</i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection