@extends('layouts.panel')

@section('content')
    <script>
        //tinymce.init({ selector:'textarea' });

        tinymce.init({
            selector: 'textarea',
            height: 500,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            image_title: true,
            // enable automatic uploads of images represented by blob or data URIs
            automatic_uploads: true,
            // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
            images_upload_url: '{{route('panel.photoalbum.upload')}}',
            // here we add custom filepicker only to Image dialog
            file_picker_types: 'image',
            // and here's our custom image picker
            file_picker_callback: function(cb, value, meta) {
                var input1 = document.createElement('input');
                input1.setAttribute('name', '_token');
                input1.setAttribute('type', 'text');
                input1.setAttribute('value', '{{ csrf_token() }}');

                var input = document.createElement('input');
                input.setAttribute('name', 'file');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');

                // Note: In modern browsers input[type="file"] is functional without
                // even adding it to the DOM, but that might not be the case in some older
                // or quirky browsers like IE, so you might want to add it to the DOM
                // just in case, and visually hide it. And do not forget do remove it
                // once you do not need it anymore.

                input.onchange = function() {
                    var file = this.files[0];

                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var blobInfo = blobCache.create(id, file);
                    blobCache.add(blobInfo);

                    // call the callback and populate the Title field with the file name
                    cb(blobInfo.blobUri(), { title: file.name });
                };

                input.click();
            }
        });


    </script>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="green">
                    <i class="material-icons">add_circle_outline</i>
                </div>
                <div class="card-content">
                    <?php
                        $route = route('panel.news.store');
                        if (isset($news)) {
                            $route = route('panel.news.update',$news['id']);
                        }
                    ?>
                    <h4 class="card-title">{{ $title }}</h4>
                    <form action="{{ $route }}" method="POST" id="form1">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input id="title" type="text" class="form-control" name="title" placeholder="Название" value="{{ $news['title'] or "" }}">
                                </div>
                            </div>
                        </div>

                        <textarea name="text" rows="12">{{ $news['text'] or "" }}</textarea>

                        <div align="center">
                            <div class="form-group" style="width: 500px;" align="left">
                                <label for="created_at" class="col-sm-12 control-label">Фотоальбом</label><br>
                                <div class="col-sm-12">
                                    <select class="selectpicker" data-style="select-with-transition" title="Выберите альбом" data-size="7" name="photoalbum_id">
                                        <option value="0"> Выберите альбом</option>
                                        @foreach($albums as $album)
                                            <option value="{{ $album['id'] }}" @if(isset($news)) @if($news['photoalbum_id']==$album['id']) selected @endif @endif >{{ $album['id'] }} - {{ $album['name'] }} </option>
                                        @endforeach
                                    </select>


                                </div>
                            </div>

                            <div class="form-group" style="width: 200px;" align="left">
                                <label for="created_at" class="col-sm-12 ctitleontrol-label">Дата добавления</label><br>
                                <div class="col-sm-12">
                                    <?php
                                    $value = date('Y-m-d'); //"2000-01-01";
                                    if (isset($news)) {
                                        $value = date('Y-m-d', strtotime($news['created_at']));
                                    }
                                    ?>
                                    <input id="created_at" type="text" class="form-control datepicker" name="created_at" value="{{ $value }}" >
                                </div>
                            </div>


                            <br>
                            <br>

                            <script>
                                function save() {
                                    if ($("#title").val() == "") {
                                        alert("Введите название новости");
                                    } else {
                                        $("#form1").submit();
                                    }
                                }
                            </script>

                            <div class="btn btn-success" value="" style="width: 70%" onclick="save()">Сохранить</div>
                            <div class="" style="width: 200px;" align="left">
                                <div class="col-sm-12">
                                    <div class="checkbox checkbox-inline">
                                        <label>

                                            <?php
                                            $value = "";
                                            if (isset($news)) {
                                                if ($news['draft'] == true)
                                                    $value = "checked";
                                            }
                                            ?>
                                            <input type="checkbox" name="draft" {{ $value }}> Черновик
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection