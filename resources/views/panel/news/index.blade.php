@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="blue">
                    <i class="material-icons">chrome_reader_mode</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">{{ $title }}</h4>
                    <div class="row">
                        <div class="col-md-12" align="right">

                            <a href="{{ route('panel.news.create') }}" class="btn btn btn-success" >
                                <i class="material-icons">add_circle_outline</i> Добавить новость
                            </a>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Название</th>
                                            <th>Дата добавления</th>
                                            <th>Действия</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($news as $new)
                                            <tr>
                                                <td>
                                                    <a href="{{ $new->getUrl()  }}" target="_blank">
                                                        {{ $new['title'] }}
                                                    </a>
                                                </td>
                                                <td>{{ $new->getDate() }}</td>
                                                <td>
                                                    <a href="{{ route('panel.news.edit', $new['id']) }}" class="btn btn-sm btn-info" style="padding: 5px;margin: 0;">
                                                        <i class="material-icons">mode_edit</i>
                                                    </a>
                                                    <a href="{{ route('panel.news.destroy', $new['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                                        <i class="material-icons">delete_forever</i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection