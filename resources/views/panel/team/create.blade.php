@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="green">
                    <i class="material-icons">add_circle_outline</i>
                </div>
                <div class="card-content">
                    <?php
                    $route = route('panel.team.store');
                    if (isset($team)) {
                        $route = route('panel.team.update',$team['id']);
                    }
                    ?>
                    <h4 class="card-title">{{ $title }}</h4>
                    <form action="{{ $route }}" method="POST"  class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <label class="col-sm-2 label-on-left">Название</label>
                            <div class="col-sm-10">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input type="text" class="form-control" name="name" placeholder="Название" value="{{ $team['name'] or "" }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 label-on-left">Логотип</label>
                            <div class="col-sm-10">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            @if(isset($team) && $team['logotype_url'] != '')
                                                <img src="{{ $team['logotype_url'] }}" alt="...">
                                            @else
                                                <img src="/assets_panel/img/image_placeholder.jpg" alt="...">
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                        <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Выбрать</span>
                                                        <span class="fileinput-exists">Сменить</span>
                                                        <input type="file" name="logotype_photo" />
                                                    </span>
                                            <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Удалить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <label class="col-sm-2 label-on-left">Сторона</label>
                            <div class="col-sm-10 checkbox-radios">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="enemy" value="0" <?php if(isset($team)) { if ($team['enemy']==0) echo "checked";} else echo "checked"?>> Наша команда
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="enemy" value="1" <?php if(isset($team)) if ($team['enemy']==1) echo "checked";?>> Команда соперников
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 label-on-left">Арена</label>
                            <div class="col-sm-10" style="padding-top: 6px">
                                    <select class="selectpicker" data-style="select-with-transition" title="Выберите арену" data-size="7" name="arena_id">
                                        <option disabled> Выберите арену</option>
                                        @foreach($arenas as $arena)
                                            <option value="{{ $arena['id'] }}" <?php if(isset($team)) if ($team['arena_id'] == $arena['id']) echo "selected "?>>{{ $arena['id'] }} - {{ $arena['name'] }} </option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>






                        <div align="center">
                            <input type="submit" class="btn btn-success" value="Сохранить" style="width: 70%">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    @if(isset($players))
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Игроки этой команды</h4>
                        <div class="row">
                            <div class="col-md-12" align="right">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Ф.И.О.</th>
                                            <th>Позиция</th>
                                            <th>Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($players as $player)
                                            <tr>
                                                <td>
                                                    <a href="{{ route('player.show', $player['id']) }}" target="_blank">
                                                        {{ $player->getFio() }}
                                                    </a>
                                                </td>
                                                <td>{{ $player->getPositionName() }}</td>
                                                <td>
                                                    <a href="{{ route('panel.player.edit', $player['id']) }}" class="btn btn-sm btn-info" style="padding: 5px;margin: 0;">
                                                        <i class="material-icons">mode_edit</i>
                                                    </a>
                                                    <a href="{{ route('panel.player.destroy', $player['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                                        <i class="material-icons">delete_forever</i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


@endsection