@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="red">
                    <i class="material-icons">group</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">{{ $title }}</h4>
                    <div class="row">
                        <div class="col-md-12" align="right">

                            <a href="{{ route('panel.team.create') }}" class="btn btn btn-success" >
                                <i class="material-icons">add_circle_outline</i> Добавить команду
                            </a>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Арена</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($teams as $team)
                                        <tr>
                                            <td>
                                                <a href="{{ route('team.show', $team['id']) }}" target="_blank">
                                                    {{ $team['name'] }}
                                                </a>
                                            </td>
                                            <td>{{ $team->getArenaName() }}</td>
                                            <td>
                                                <a href="{{ route('panel.team.edit', $team['id']) }}" class="btn btn-sm btn-info" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                                <a href="{{ route('panel.team.destroy', $team['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">delete_forever</i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection