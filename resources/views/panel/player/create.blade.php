@extends('layouts.panel')

@section('content')

    <div class="col-md-12">
        <div class="card">
            <form method="POST" action="{{ $route }}" class="form-horizontal" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-header card-header-text" data-background-color="blue">
                    <i class="material-icons">person</i>
                </div>
                <div class="card-content">


                    @foreach($fields_1 as $field)
                        <div class="row">
                            <label class="col-sm-2 label-on-left">{{ $field['title'] }}</label>
                            <div class="col-sm-10">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input type="text" class="form-control" name="{{ $field['name'] }}" value="{{ $player[$field['name']] or "" }}">
                                    <span class="help-block">{{ $field['desc'] or $field['title'] }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="row">
                        <label class="col-sm-2 label-on-left">Дата рождения</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label"></label>
                                <?php
                                $value = "2000-01-01";
                                if (isset($player)) {
                                    $value = date('Y-m-d', strtotime($player['birthday']));
                                }
                                ?>
                                <input id="birthday" type="text" class="form-control datepicker" name="birthday" value="{{ $value }}" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 label-on-left">Фотография</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label"></label>
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        @if(isset($player) && $player['photo_url'] != '')
                                            <img src="{{ $player['photo_url'] }}" alt="...">
                                        @else
                                            <img src="/assets_panel/img/image_placeholder.jpg" alt="...">
                                        @endif
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div>
                                                <span class="btn btn-rose btn-round btn-file">
                                                    <span class="fileinput-new">Выбрать</span>
                                                    <span class="fileinput-exists">Сменить</span>
                                                    <input type="file" name="player_photo" />
                                                </span>
                                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Удалить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 label-on-left">Позиция</label>
                        <div class="col-sm-10 checkbox-radios">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="position" value="1" <?php if(isset($player)) { if ($player['position']==1) echo "checked";} else echo "checked";?> > Вратарь
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="position" value="2" <?php if(isset($player)) if ($player['position']==2) echo "checked";?> > Защитник
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="position" value="3"  <?php if(isset($player)) if ($player['position']==3) echo "checked";?> > Нападающий
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 label-on-left">Команды</label>
                        <div class="col-sm-10 checkbox-radios">
                            @foreach($teams as $team)
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="team-{{ $team['id'] }}"  @if($player_teams->contains($team)) checked @endif> {{ $team['name'] }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text" class="form-control" name="throw-{{$team['id']}}" value="{{ $default_stats[$team['id']]['throw'] or 0 }}">
                                            <span class="help-block">Голы</span>
                                            <span class="material-input"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text" class="form-control" name="asist-{{$team['id']}}" value="{{ $default_stats[$team['id']]['asist'] or 0 }}">
                                            <span class="help-block">Асисты</span>
                                            <span class="material-input"></span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 label-on-left">Видмость</label>
                        <div class="col-sm-10 checkbox-radios">
                            <div class="checkbox">
                                <label>
                                    <?php
                                    $value = "";
                                    if (isset($player)) {
                                        if ($player['in_team'] == true)
                                            $value = "checked";
                                    }
                                    ?>
                                    <input type="checkbox" name="in_team" {{ $value }}> Игрок находится в команде
                                </label>
                            </div>
                        </div>
                    </div>




                    @foreach($fields_2 as $field)
                        <div class="row">
                            <label class="col-sm-2 label-on-left">{{ $field['title'] }}</label>
                            <div class="col-sm-10">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input type="text" class="form-control" name="{{ $field['name'] }}"  value="{{ $player[$field['name']] or "" }}">
                                    <span class="help-block">{{ $field['desc'] or $field['title'] }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="row" align="center">
                        <input type="submit" class="btn btn-success" value="Сохранить" style="width:70%">
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection