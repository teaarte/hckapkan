@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="green">
                    <i class="material-icons">person</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">{{ $title }}</h4>
                    <div class="row">
                        <div class="col-md-12" align="right">

                            <a href="{{ route('panel.player.create') }}" class="btn btn btn-success" >
                                <i class="material-icons">add_circle_outline</i> Добавить игрока
                            </a>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Ф.И.О.</th>
                                        <th>Дата рождения</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($players as $player)
                                        <tr>
                                            <td>
                                                <a href="{{ route('player.show', $player['id']) }}" target="_blank">
                                                    {{ $player->getFio() }}
                                                </a>
                                            </td>
                                            <td>{{ $player->getBirthday() }}</td>
                                            <td>
                                                <a href="{{ route('panel.player.edit', $player['id']) }}" class="btn btn-sm btn-info" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                                <a href="{{ route('panel.player.destroy', $player['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">delete_forever</i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection