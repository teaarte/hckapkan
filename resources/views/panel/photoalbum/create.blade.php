@extends('layouts.panel')

@section('content')

    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-text" data-background-color="blue">
                <i class="material-icons">person</i>
            </div>
            <div class="card-content">
                <form method="POST" action="{{ $route }}" class="form-horizontal">
                    {{ csrf_field() }}

                    <div class="row">
                        <label class="col-sm-2 label-on-left">Название альбома</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" name="name" placeholder="Название альбома" value="{{ $photoalbum['name'] or "" }}">
                                <span class="help-block">Название альбома</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 label-on-left">Дата добавления</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label"></label>
                                <?php
                                $value = date('Y-m-d'); //"2000-01-01";
                                if (isset($photoalbum)) {
                                    $value = date('Y-m-d', strtotime($photoalbum['created_at']));
                                }
                                ?>
                                <input id="created_at" type="text" class="form-control datepicker" name="created_at" value="{{ $value }}" >

                            </div>
                        </div>
                    </div>


                    <div class="row" align="center">
                        <input type="submit" class="btn btn-success" value="Сохранить" style="width:70%">
                    </div>


                </form>



            </div>
        </div>
    </div>


@endsection