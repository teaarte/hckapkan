@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="orange">
                    <i class="material-icons">photo_library</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">{{ $title }}</h4>
                    <div class="row">
                        <div class="col-md-12" align="right">

                            <a href="{{ route('panel.photoalbum.create') }}" class="btn btn btn-success" >
                                <i class="material-icons">add_circle_outline</i> Добавить альбом
                            </a>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Кол-во фото</th>
                                        <th>Дата</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($photoalbums as $photoalbum)
                                        <tr>
                                            <td>
                                                {{ $photoalbum['name'] }}
                                            </td>
                                            <td>
                                                {{ $photoalbum->photos()->get()->count() }}
                                            </td>
                                            <td>{{ $photoalbum['created_at'] }}</td>
                                            <td>
                                                <a href="{{ route('panel.photoalbum.show', $photoalbum['id']) }}" class="btn btn-sm btn-warning" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">photo_camera</i>
                                                </a>
                                                <a href="{{ route('panel.photoalbum.edit', $photoalbum['id']) }}" class="btn btn-sm btn-info" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                                <a href="{{ route('panel.photoalbum.destroy', $photoalbum['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">delete_forever</i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection