@extends('layouts.panel')

@section('content')

    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-text" data-background-color="blue">
                <i class="material-icons">person</i>
            </div>
            <div class="card-content">
                <form action="{{route('panel.photoalbum.upload')}}" method="POST" class="dropzone">
                    {{ csrf_field() }}
                    <input type="hidden" name="album_id" value="{{ $photoalbum['id'] }}">
                </form>


                <h3 align="center">Фотографии</h3>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Фото</th>
                            <th style="text-align: center">Удалить</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($photos as $photo)
                            <tr>
                                <td>
                                    <img src="{{ $photo['url'] }}" alt="" style="max-width: 200px;">
                                </td>
                                <td style="text-align: center">
                                    <a href="{{ route('panel.photoalbum.destroy_photo', $photo['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                        <i class="material-icons">delete_forever</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>


@endsection