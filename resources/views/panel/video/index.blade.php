@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="orange">
                    <i class="material-icons">video_library</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">{{ $title }}</h4>
                    <div class="row">
                        <div class="col-md-12" align="right">

                            <a href="{{ route('panel.video.create') }}" class="btn btn btn-success" >
                                <i class="material-icons">add_circle_outline</i> Добавить видео
                            </a>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Дата</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($videos as $video)
                                        <tr>
                                            <td>
                                                {{ $video['name'] }}
                                            </td>
                                            <td>{{ $video['created_at'] }}</td>
                                            <td>
                                                <a href="{{ route('panel.video.edit', $video['id']) }}" class="btn btn-sm btn-info" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                                <a href="{{ route('panel.video.destroy', $video['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">delete_forever</i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection