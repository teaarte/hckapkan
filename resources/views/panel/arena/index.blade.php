@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="orange">
                    <i class="material-icons">add_location</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">{{ $title }}</h4>
                    <div class="row">
                        <div class="col-md-12" align="right">

                            <a href="{{ route('panel.arena.create') }}" class="btn btn btn-success" >
                                <i class="material-icons">add_circle_outline</i> Добавить арену
                            </a>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Арена</th>
                                        <th>Адрес</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($arenas as $arena)
                                        <tr>
                                            <td>{{ $arena['id'] }}</td>
                                            <td>
                                                <a href="{{ route('panel.arena.edit', $arena['id']) }}" target="_blank">
                                                    {{ $arena['name'] }}
                                                </a>
                                            </td>
                                            <td>{{ $arena['address'] }}</td>
                                            <td>
                                                <a href="{{ route('panel.arena.edit', $arena['id']) }}" class="btn btn-sm btn-info" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                                <a href="{{ route('panel.arena.destroy', $arena['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">delete_forever</i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection