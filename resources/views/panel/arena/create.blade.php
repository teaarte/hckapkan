@extends('layouts.panel')

@section('content')

    <div class="col-md-12">
        <div class="card">

            <?php
            $route = route('panel.arena.store');
            if (isset($arena)) {
                $route = route('panel.arena.update',$arena['id']);
            }
            ?>

            <form method="POST" action="{{ $route }}" class="form-horizontal">
                {{ csrf_field() }}
                <div class="card-header card-header-text" data-background-color="blue">
                    <i class="material-icons">add_location</i>
                </div>
                <div class="card-content">


                    <div class="row">
                        <label class="col-sm-2 label-on-left">Название</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" name="name" value="{{ $arena['name'] or "" }}">
                                <span class="help-block">Введите название арены</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 label-on-left">Краткое название</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" name="short_name" value="{{ $arena['short_name'] or "" }}">
                                <span class="help-block">Введите краткое название арены</span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <label class="col-sm-2 label-on-left">Адрес</label>
                        <div class="col-sm-10">
                            <div class="form-group label-floating is-empty">
                                <label class="control-label"></label>
                                <input type="text" class="form-control" name="address" value="{{ $arena['address'] or "" }}">
                                <span class="help-block">Введите адрес арены</span>
                            </div>
                        </div>
                    </div>

                    <div class="row" align="center">
                        <input type="submit" class="btn btn-success" value="Сохранить" style="width:70%">
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection