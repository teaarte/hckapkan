@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="green">
                    <i class="material-icons">add_circle_outline</i>
                </div>
                <div class="card-content">
                    <?php
                    $route = route('panel.calendar.store');
                    if (isset($calendar)) {
                        $route = route('panel.calendar.update',$calendar['id']);
                    }
                    ?>
                    <h4 class="card-title">{{ $title }}</h4>
                    <form action="{{ $route }}" method="POST"  class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="row">
                            <label class="col-sm-2 label-on-left">Название</label>
                            <div class="col-sm-10">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input type="text" class="form-control" name="name" placeholder="Название" value="{{ $calendar['name'] or "" }}">
                                </div>
                            </div>
                        </div>

                        <div align="center">
                            <input type="submit" class="btn btn-success" value="Сохранить" style="width: 70%">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection