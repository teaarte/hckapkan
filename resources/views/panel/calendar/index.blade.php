@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="red">
                    <i class="material-icons">date_range</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">{{ $title }}</h4>
                    <div class="row">
                        <div class="col-md-12" align="right">

                            <a href="{{ route('panel.calendar.create') }}" class="btn btn btn-success" >
                                <i class="material-icons">add_circle_outline</i> Добавить календарь
                            </a>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Название</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($calendars as $calendar)
                                        <tr>
                                            <td>
                                                <a href="{{ route('panel.calendar.match', $calendar['id']) }}">
                                                    {{ $calendar['name'] }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ route('panel.calendar.up', $calendar['id']) }}" class="btn btn-sm btn-default" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">keyboard_arrow_up</i>
                                                </a>
                                                <a href="{{ route('panel.calendar.down', $calendar['id']) }}" class="btn btn-sm btn-default" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">keyboard_arrow_down</i>
                                                </a>

                                                <a href="{{ route('panel.calendar.edit', $calendar['id']) }}" class="btn btn-sm btn-info" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                                <a href="{{ route('panel.calendar.destroy', $calendar['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">delete_forever</i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection