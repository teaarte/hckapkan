<form action="{{ route('panel.match.store3', $match['id']) }}" method="post" id="form3">
    {{ csrf_field() }}
    @for($i=1; $i<=2; $i++)
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <div class="col-md-12">
                        <h4 align="center">Шайбы команды {{ $match->getTeamName($i) }}</h4>
                        <?php
                        $players = $match->getMatchteam($i)->players()->get();
                        $players_string = '';
                        $goals = $match->getMatchgoals()->get()->where('matchteam_id', $match['team'.$i.'_id']);
                        $goals_count = 0;
                        ?>
                        @foreach($goals as $goal)
                            <?php $goals_count++; ?>
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-1">
                                    Минута
                                    <input type="text" name="{{$i}}_{{$goals_count}}_minute" value="{{ $goal['minute'] }}" style="width: 100%">
                                </div>
                                <div class="col-sm-3">
                                    Забил
                                    @if($players->count() == 0)
                                        <input type="text" name="{{$i}}_{{$goals_count}}_throw" value="{{ $goal['throw'] }}" style="width: 100%">
                                    @else
                                        <select name="{{$i}}_{{$goals_count}}_throw_id" style="width: 100%">
                                            @foreach ($players as $player)
                                                <option value="{{$player['id']}}" @if($player['id'] == $goal['throw_id']) selected @endif>{{$player->getNameSurname()}}</option>';
                                            @endforeach
                                        </select>
                                    @endif
                                </div>

                                <div class="col-sm-3">
                                    Асист 1
                                    @if($players->count() == 0)
                                        <input type="text" name="{{$i}}_{{$goals_count}}_asist1" value="{{ $goal['asist1'] }}" style="width: 100%">
                                    @else
                                        <select name="{{$i}}_{{$goals_count}}_asist1_id" style="width: 100%">
                                            <option value="0">Нет</option>
                                            @foreach ($players as $player)
                                                <option value="{{$player['id']}}" @if($player['id'] == $goal['asist1_id']) selected @endif>{{$player->getNameSurname()}}</option>';
                                            @endforeach
                                        </select>
                                    @endif
                                </div>

                                <div class="col-sm-3">
                                    Асист 2
                                    @if($players->count() == 0)
                                        <input type="text" name="{{$i}}_{{$goals_count}}_asist2"  value="{{ $goal['asist2'] }}" style="width: 100%">
                                    @else
                                        <select name="{{$i}}_{{$goals_count}}_asist2_id" style="width: 100%">
                                            <option value="0">Нет</option>
                                            @foreach ($players as $player)
                                                <option value="{{$player['id']}}" @if($player['id'] == $goal['asist2_id']) selected @endif>{{$player->getNameSurname()}}</option>';
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                            <?php
                            if ($goals_count == $match['team'.$i.'_goals']) {
                                break 1;
                            }
                            ?>
                        @endforeach
                        @for($j = $goals_count+1; $j <= $match['team'.$i.'_goals']; $j++)
                            <div class="row" style="margin-bottom: 10px">
                                <div class="col-sm-1">
                                    Минута
                                    <input type="text" name="{{$i}}_{{$j}}_minute" value="" style="width: 100%">
                                </div>
                                <div class="col-sm-3">
                                    Забил
                                    @if($players->count() == 0)
                                        <input type="text" name="{{$i}}_{{$j}}_throw" value="" style="width: 100%">
                                    @else
                                        <select name="{{$i}}_{{$j}}_throw_id" style="width: 100%">
                                            @foreach ($players as $player)
                                                <option value="{{$player['id']}}">{{$player->getNameSurname()}}</option>';
                                            @endforeach
                                        </select>
                                    @endif
                                </div>

                                <div class="col-sm-3">
                                    Асист 1
                                    @if($players->count() == 0)
                                        <input type="text" name="{{$i}}_{{$j}}_asist1" value="" style="width: 100%">
                                    @else
                                        <select name="{{$i}}_{{$j}}_asist1_id" style="width: 100%">
                                            <option value="0">Нет</option>
                                            @foreach ($players as $player)
                                                <option value="{{$player['id']}}">{{$player->getNameSurname()}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>

                                <div class="col-sm-3">
                                    Асист 2
                                    @if($players->count() == 0)
                                        <input type="text" name="{{$i}}_{{$j}}_asist2" value="" style="width: 100%">
                                    @else
                                        <select name="{{$i}}_{{$j}}_asist2_id" style="width: 100%">
                                            <option value="0">Нет</option>
                                            @foreach ($players as $player)
                                                <option value="{{$player['id']}}">{{$player->getNameSurname()}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    @endfor
</form>


<script>
    function save_step3(completion) {
        console.log('save step 3');
        //

        $.ajax({
            type: 'post',
            url: $('#form3').attr('action'),
            data: $('#form3').serialize(),
            success: function () {

            }
        }).done(function (data) {
            console.log(data);
            alert('Голы записаны');
            completion();
        }).fail(function (data) {
            console.log(data);
            alert('Ошибка');
        });

    }
</script>