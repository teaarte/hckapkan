@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="orange">
                    <i class="material-icons">monetization_on</i>
                </div>
                <div class="card-content">
                    <h4 class="card-title">{{ $title }}</h4>
                    <div class="row">
                        <div class="col-md-12" align="right">

                            <a href="{{ route('panel.match.create') }}" class="btn btn btn-success" >
                                <i class="material-icons">add_circle_outline</i> Добавить матч
                            </a>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Команда 1</th>
                                        <th>Команда 2</th>
                                        <th>Дата</th>
                                        <th>Арена</th>
                                        <th>Статус</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($matches as $match)
                                        <tr>
                                            <td>{{ $match->getTeamName(1) }}</td>
                                            <td>{{ $match->getTeamName(2) }}</td>
                                            <td>{{ $match['date'] }}</td>
                                            <td>{{ $match->getArenaName() }}</td>
                                            <td>{{ $match->getStatusName() }}</td>
                                            <td>
                                                <a href="{{ route('panel.match.edit', $match['id']) }}" class="btn btn-sm btn-info" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                                <a href="{{ route('panel.match.destroy', $match['id']) }}" class="btn btn-sm btn-danger" style="padding: 5px;margin: 0;">
                                                    <i class="material-icons">delete_forever</i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection