@extends('layouts.panel')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="green">
                    <i class="material-icons">add_circle_outline</i>
                </div>
                <div class="card-content">
                    <?php
                    $route = route('panel.match.store');
                    if (isset($match)) {
                        $route = route('panel.match.update',$match['id']);
                    }
                    ?>
                    <h4 class="card-title">{{ $title }}</h4>
                    <form action="{{ $route }}" method="POST" class="form-horizontal" id="form1">
                        {{ csrf_field() }}
                        <input type="hidden" id="team_players" name="team_players" value="">
                        <input type="hidden" id="finished_input" name="finished" value="">

                        <script>

                            var _players = {
                                1: [

                                ],
                                2: [

                                ],
                            };
                            var team_players = {
                                1: [
                                    @if(isset($match))
                                    @foreach($match->getMatchteam(1)->players()->get() as $player)
                                    {{$player['id']}},
                                    @endforeach
                                    @endif
                                ],
                                2: [
                                    @if(isset($match))
                                    @foreach($match->getMatchteam(2)->players()->get() as $player)
                                    {{$player['id']}},
                                    @endforeach
                                    @endif
                                ],
                            };
                            var goals = {
                                1: [

                                ],
                                2: [

                                ],
                            };


                            var arena = 0;
                            var calendar = 0;
                            var finished = @if(isset($match)) @if($match['finished']==1) 1 @else 0 @endif @else 0 @endif ;
                            var match_id = {{ $match['id'] or 0 }};

                            function finishChanged(checked) {
                                if (checked)
                                    finished = 1;
                                else
                                    finished = 0;
                            }

                            var now_request = false;

                            function sendData() {
                                if (!now_request) {
                                    now_request = true;
                                    $('#team_players').val(JSON.stringify(team_players));
                                    $('#finished_input').val(finished);
                                    var route = '{{route('panel.match.store')}}';
                                    if (match_id != 0) {
                                        route += '/edit/' + match_id;
                                    }
                                    $.ajax({
                                        type: 'post',
                                        url: route,
                                        data: $('#form1').serialize(),
                                        success: function () {
                                            now_request = false;
                                        }
                                    }).done(function (data) {
                                        console.log(data);
                                        now_request = false;
                                        if (match_id == 0) {
                                            alert('Матч создан');
                                        } else {
                                            alert('Матч изменён');
                                        }
                                        match_id = data;
                                        load_step_2();
                                        load_step_3();
                                        load_step_4();
                                    }).fail(function (data) {
                                        now_request = false;
                                        console.log(data);
                                        alert('Ошибка');
                                    });
                                }
                            }

                            function selectPlayer(team, player_id, add) {
                                if (add == false) {
                                    var index = team_players[team].indexOf(player_id);
                                    team_players[team].splice(index, 1);
                                } else {
                                    team_players[team].push(player_id);
                                }
                                console.log(team_players);
                                //console.log(team+" "+player_id+" "+remove);
                            }

                            function selectArena() {
                                var selectBox = document.getElementById("arena_id");
                                arena = selectBox.options[selectBox.selectedIndex].value;
                            }

                            function selectCalendar() {
                                var selectBox = document.getElementById("calendar");
                                calendar = selectBox.options[selectBox.selectedIndex].value;
                            }

                            function selectTeam(number, noclear) {
                                var selectBox = document.getElementById("team"+number+"_id");
                                var selectedValue = selectBox.options[selectBox.selectedIndex].value;
                                $.get("{{ route('api') }}/team/"+selectedValue, function(data) {
                                    if (noclear != 1) team_players[number] = [];
                                    var posNames = ["Вратари", "Защитники", "Нападающие"];
                                    var positions = [];
                                    for (var i = 1; i <= 3; i++) {
                                        positions[i] = $('#position_'+number+'_'+i);
                                        positions[i].empty();
                                        positions[i].append('<div align="center">'+posNames[i-1]+'</div>');
                                    }
                                    var players = JSON.parse(data);
                                    if (players.length > 0) {
                                        $('#team' + number).show();
                                    } else {
                                        $('#team' + number).hide();
                                    }
                                    _players[number] = players;
                                    players.forEach(function (player) {
                                        var playerText = player.surname+" "+player.name;
                                        if (player.in_team == 1) {
                                            playerText = "<span style=\"color: #000\">"+ playerText+"</span>";
                                        }
                                        var index = team_players[number].indexOf(player.id);
                                        var checked = '';
                                        if (index != -1) checked = 'checked';
                                        positions[player.position].append('<div class=""> <label> <input type="checkbox" '+checked+' onclick="selectPlayer('+number+', '+player.id+', this.checked)"> '+playerText+ '</label> </div>');
                                    });
                                    //console.log( json );
                                });
                            }

                            $(document).ready(function() {
                                console.log();
                                @if(isset($match))
                                setTimeout(function () {
                                    var tmp = team_players;
                                    selectTeam(1, 1);
                                    selectTeam(2, 1);
                                    team_players = tmp;
                                    load_step_2();
                                    load_step_3();
                                    load_step_4();
                                }, 100);

                                @endif
                            });

                        </script>


                        <div class="card" style="margin-top: 0">
                            <div class="card-content">

                                <div class="row">
                                    <label class="col-sm-2 label-on-left">Календарь</label>
                                    <div class="col-sm-10" style="padding-top: 6px">
                                        <select id="calendar_id" class="selectpicker" data-style="select-with-transition" title="Выберите календарь" data-size="7" name="calendar_id">
                                            <option value="0" disabled> Выберите календарь</option>
                                            @foreach($calendars as $calendar)
                                                <option value="{{ $calendar['id'] }}" @if(isset($match)) @if($calendar['id']==$match['calendar_id']) selected @endif @endif>{{ $calendar['id'] }} - {{ $calendar['name'] }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <br>

                                <div class="row">
                                    <label class="col-sm-2 label-on-left">Команда 1</label>
                                    <div class="col-sm-10" style="padding-top: 6px">
                                        <select id="team1_id" class="selectpicker" data-style="select-with-transition" title="Выберите команду 1" data-size="7" name="team1_id" onchange="selectTeam(1)">
                                            <option disabled> Выберите команду 1</option>
                                            @foreach($teams as $team)
                                                <option value="{{ $team['id'] }}"  @if(isset($match)) @if($team['id']==$match->getTeam(1)['id']) selected @endif @endif>{{ $team['id'] }} - {{ $team['name'] }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <br>


                                <div class="row" id="team1"  style="display:none;">
                                    <div class="text-center">
                                        <h4>Состав команды 1</h4>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card" style="margin-top: 0">
                                            <div class="card-content" id="position_1_1">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card" style="margin-top: 0">
                                            <div class="card-content" id="position_1_2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card" style="margin-top: 0">
                                            <div class="card-content" id="position_1_3">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <br>


                                <div class="row">
                                    <label class="col-sm-2 label-on-left">Команда 2</label>
                                    <div class="col-sm-10" style="padding-top: 6px">
                                        <select id="team2_id" class="selectpicker" data-style="select-with-transition" title="Выберите команду 2" data-size="7" name="team2_id" onchange="selectTeam(2)">
                                            <option disabled> Выберите команду 2</option>
                                            @foreach($teams as $team)
                                                <option value="{{ $team['id'] }}" @if(isset($match)) @if($team['id']==$match->getTeam(2)['id']) selected @endif @endif> {{ $team['id'] }} - {{ $team['name'] }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <br>

                                <div class="row" id="team2"  style="display:none;">
                                    <div class="text-center">
                                        <h4>Состав команды 2</h4>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card" style="margin-top: 0">
                                            <div class="card-content" id="position_2_1">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card" style="margin-top: 0">
                                            <div class="card-content" id="position_2_2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card" style="margin-top: 0">
                                            <div class="card-content" id="position_2_3">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <br>

                                <div class="row">
                                    <label class="col-sm-2 label-on-left">Арена</label>
                                    <div class="col-sm-10" style="padding-top: 6px">
                                        <select id="arena_id" class="selectpicker" data-style="select-with-transition" title="Выберите арену" data-size="7" name="arena_id" onchange="selectArena()">
                                            <option value="0" disabled> Выберите арену</option>
                                            @foreach($arenas as $arena)
                                                <option value="{{ $arena['id'] }}"  @if(isset($match)) @if($arena['id']==$match['arena_id']) selected @endif @endif>{{ $arena['id'] }} - {{ $arena['name'] }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <label class="col-sm-2 label-on-left">Дата и время</label>
                                    <div class="col-sm-10">
                                        <input id="date" type="text" class="form-control datetimepicker" name="date" @if(isset($match)) value="{{ $match->getDate2() }}" @endif>
                                    </div>
                                </div>


                                <div class="row">
                                    <label class="col-sm-2 label-on-left">Завершён ли матч?</label>
                                    <div class="col-sm-10 checkbox-radios">
                                        <div class="checkbox">
                                            <label>
                                                <input id="finished" type="checkbox" onclick="finishChanged(this.checked)" @if(isset($match)) @if($match['finished']==1) checked @endif @endif > Завершён
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div align="center">
                                    <a class="btn btn-success" style="width: 70%" onclick="sendData()">Сохранить</a>
                                </div>

                            </div>
                        </div>
                        <br>

                    </form>


                    <script>
                        function load_step_2() {
                            console.log('load step 2');
                            $.ajax({
                                url : '{{ route("panel.match") }}/create2/'+match_id,
                            }).done(function (data) {
                                $('#st_2').html(data);
                                if (data != '') {
                                    $('#st_2_btn').show();
                                } else {
                                    $('#st_2_btn').hide();
                                }
                            }).fail(function () {
                                alert('could not be loaded222.');
                            });
                        }
                    </script>

                    <div class="card" style="margin-top: 0">
                        <div class="card-content" >
                            <div id="st_2"></div>
                            <div class="row" align="center">
                                <a id="st_2_btn" class="btn btn-success" style="width: 70%; display: none;"  onclick="save_step2(load_step_3)">Сохранить</a>
                            </div>
                        </div>

                    </div>



                    <script>
                        function load_step_3() {
                            console.log('load step 3');
                            $.ajax({
                                url : '{{ route("panel.match") }}/create3/'+match_id,
                            }).done(function (data) {
                                $('#st_3').html(data);
                                if (data != '') {
                                    $('#st_3_btn').show();
                                } else {
                                    $('#st_3_btn').hide();
                                }
                            }).fail(function () {
                                alert('could not be loaded.');
                            });
                        }
                    </script>


                    <div class="card" style="margin-top: 0">
                        <div class="card-content" >
                            <div id="st_3"></div>
                            <div class="row" align="center">
                                <a id="st_3_btn" class="btn btn-success" style="width: 70%; display: none"  onclick="save_step3(load_step_3)">Сохранить</a>
                            </div>
                        </div>

                    </div>




                    <script>
                        function load_step_4() {
                            console.log('load step 4');
                            $.ajax({
                                url : '{{ route("panel.match") }}/photo/'+match_id,
                            }).done(function (data) {
                                $('#st_4').html(data);
                                if (data != '') {
                                    $('#st_4_btn').show();
                                } else {
                                    $('#st_4_btn').hide();
                                }
                            }).fail(function () {
                                alert('could not be loaded.');
                            });
                        }
                    </script>
                    <div class="card" style="margin-top: 0">
                        <div class="card-content" >
                            <div id="st_4"></div>
                            <div class="row" align="center">
                                <a id="st_4_btn" class="btn btn-success" style="width: 70%; display: none"  onclick="save_step4(load_step_4)">Сохранить</a>
                            </div>
                        </div>

                    </div>




                </div>
            </div>
        </div>
    </div>
@endsection