
<form action="{{ route('panel.match.photo_store', $match['id']) }}"  id="form4" method="POST">
    {{ csrf_field() }}

    <div class="form-group" align="left">
        <label for="created_at" class="col-sm-12 control-label">Фотоальбом</label><br>
        <div class="col-sm-12">
            <select class="" data-style="select-with-transition" title="Выберите альбом" data-size="7" name="photo_id">
                <option value="0"> Выберите альбом</option>
                @foreach($albums as $album)
                    <option value="{{ $album['id'] }}" @if(isset($match)) @if($match['photo_id']==$album['id']) selected @endif @endif >{{ $album['id'] }} - {{ $album['name'] }} </option>
                @endforeach
            </select>
        </div>
    </div>

    <br>
    <br>

    <div class="form-group" align="left">
        <label for="created_at" class="col-sm-12 control-label">Видео</label><br>
        <div class="col-sm-12">
            <select class="" data-style="select-with-transition" title="Выберите видео" data-size="7" name="video_id">
                <option value="0"> Выберите видео</option>
                @foreach($videos as $video)
                    <option value="{{ $video['id'] }}" @if(isset($match)) @if($match['video_id']==$video['id']) selected @endif @endif >{{ $video['id'] }} - {{ $video['name'] }} </option>
                @endforeach
            </select>
        </div>
    </div>

    <br>
    <br>
    <br>

</form>



<script>
    function save_step4(completion) {
        console.log('save step 4');

        $.ajax({
            type: 'post',
            url: $('#form4').attr('action'),
            data: $('#form4').serialize(),
            success: function () {

            }
        }).done(function (data) {
            alert('Медиа сохранены');
            completion();
        }).fail(function (data) {
            alert('Ошибка');
        });

    }
</script>