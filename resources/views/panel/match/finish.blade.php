
<div class="card-content">
    <h4 class="card-title">{{ $title }}</h4>

    <div id="step1">
        <div class="row">
            @for($i=1; $i<=2; $i++)
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-content">
                            <h4 align="center">{{ $match->getTeamName($i) }}</h4>
                            <div class="row form-horizontal">
                                <label class="col-sm-2 label-on-left">Счёт</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label"></label>
                                        <input type="text" class="form-control" name="goals_{{ $i }}" id="goals_{{ $i }}" style="text-align: center">
                                        <span class="help-block">Введите сколько шайб забросила команда {{ $match->getTeamName($i) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
        <div class="row" align="center">
            <a class="btn btn-success btn-lg" onclick="step1finished()">Далее</a>
        </div>
    </div>

    <script>
        function step2finished() {
            var goals_1 = $('#goals_1').val();
            var goals_2 = $('#goals_2').val();

            var final = {
                1: {
                    "count": goals_1,
                    "goals": [],
                },
                2: {
                    "count": goals_2,
                    "goals": [],
                }
            }
            for (var i = 0; i < goals_1; i++) {
                var goals = {
                    "min": $('#min_1_'+i).val(),
                }
                if (document.getElementById('asistid1_1_'+i)==null) {
                    goals.throw =  $('#throw_1_'+i).val();
                    goals.asist1 = $('#asist1_1_'+i).val();
                    goals.asist2 = $('#asist2_1_'+i).val();
                } else {
                    goals.throwid =  $('#throwid_1_'+i).val();
                    goals.asistid1 = $('#asistid1_1_'+i).val();
                    goals.asistid2 = $('#asistid2_1_'+i).val();
                }
                final["1"]["goals"].push(goals);
            }


            for (var i = 0; i < goals_2; i++) {
                var goals = {
                    "min": $('#min_2_'+i).val(),
                }
                if (document.getElementById('asistid1_2_'+i)==null) {
                    goals.throw =  $('#throw_2_'+i).val();
                    goals.asist1 = $('#asist1_2_'+i).val();
                    goals.asist2 = $('#asist2_2_'+i).val();
                } else {
                    goals.throwid =  $('#throwid_2_'+i).val();
                    goals.asistid1 = $('#asistid1_2_'+i).val();
                    goals.asistid2 = $('#asistid2_2_'+i).val();
                }
                final["2"]["goals"].push(goals);
            }
            console.log(final);

            $('#form1_data').val(JSON.stringify(final));
            $('#form1').submit();
        }


        function step1finished() {
            $('#step1').hide(200);
            $('#step2').show(200);

            var players_1 = [
                @foreach($match_team_1->players()->get() as $player)
                    { 'id': {{ $player['id'] }}, 'name': '{{ $player['surname'] }} {{ $player['name'] }}' },
                @endforeach
            ];
            var players_2 = [
                @foreach($match_team_2->players()->get() as $player)
                    { 'id': {{ $player['id'] }}, 'name': '{{ $player['surname'] }} {{ $player['name'] }}' },
                @endforeach
            ];

            var players_1_string = '<option value="0"></option>';
            players_1.forEach(function (player) {
                players_1_string += '<option value="'+player.id+'">'+player.name+'</option>';
            });

            var goal_1 = $('#goal_1');
            console.log(goal_1);
            goal_1.empty();
            var goals_1 = $('#goals_1').val();
            for (var i = 0; i < goals_1; i++) {
                var row = goal_1.append('<div class="row" style="margin-top: 30px"></div>');
                row.append('<div class="col-md-3">' + 'Минута <input id="min_1_'+i+'" type="text" style="width: 40px; height:20px"> </div>');
                @if($match_team_1->team()->get()->first()['enemy'] == 0)
                    row.append('<div class="col-md-3">' + "Забросил " + '<select id="throwid_1_'+i+'" >' + players_1_string + '</select></div>');
                row.append('<div class="col-md-3">' + "Асист 1 " + '<select id="asistid1_1_'+i+'">' + players_1_string + '</select></div>');
                row.append('<div class="col-md-3">' + "Асист 2 " + '<select id="asistid2_1_'+i+'">' + players_1_string + '</select></div>');
                @else
                    row.append('<div class="col-md-3">' + "Забросил " + '<input id="throw_1_'+i+'" type="text" style="width: 120px; height:20px"></div>');
                row.append('<div class="col-md-3">' + "Асист 1 " + '<input id="asist1_1_'+i+'" type="text" style="width: 120px; height:20px"></div>');
                row.append('<div class="col-md-3">' + "Асист 2 " + '<input id="asist2_1_'+i+'" type="text" style="width: 120px; height:20px"></div>');
                @endif
            }



            var players_2_string = '<option value="0"></option>';
            players_2.forEach(function (player) {
                players_2_string += '<option value="'+player.id+'">'+player.name+'</option>';
            });

            var goal_2 = $('#goal_2');
            goal_2.empty();
            var goals_2 = $('#goals_2').val();
            for (var i = 0; i < goals_2; i++) {
                var row = goal_2.append('<div class="row" style="margin-top: 30px"></div>');
                row.append('<div class="col-md-3">' + 'Минута <input id="min_2_'+i+'" type="text" style="width: 40px; height:20px"> </div>');
                @if($match_team_2->team()->get()->first()['enemy'] == 0)
                    row.append('<div class="col-md-3">' + "Забросил " + '<select id="throw_2_'+i+'" >' + players_2_string + '</select></div>');
                    row.append('<div class="col-md-3">' + "Асист 1 " + '<select id="asistid1_2_'+i+'">' + players_2_string + '</select></div>');
                    row.append('<div class="col-md-3">' + "Асист 2 " + '<select id="asistid2_2_'+i+'">' + players_2_string + '</select></div>');
                @else
                    row.append('<div class="col-md-3">' + "Забросил " + '<input id="throw_2_'+i+'" type="text" style="width: 120px; height:20px"></div>');
                    row.append('<div class="col-md-3">' + "Асист 1 " + '<input id="asist1_2_'+i+'" type="text" style="width: 120px; height:20px"></div>');
                    row.append('<div class="col-md-3">' + "Асист 2 " + '<input id="asist2_2_'+i+'" type="text" style="width: 120px; height:20px"></div>');
                @endif
            }

        }
    </script>

    <div id="step2" style="display:none;">
        @for($i=1; $i<=2; $i++)
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 align="center">Шайбы команды {{ $match->getTeamName($i) }}</h4>
                            <div class="row" id="goal_{{ $i }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endfor

        <form id="form1" action="{{ route('panel.match.finish_end', $match['id']) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="data" value="" id="form1_data">
        </form>

        <div class="row" align="center">
            <a class="btn btn-success btn-lg" onclick="step2finished()">Сохранить</a>
        </div>

    </div>

</div>