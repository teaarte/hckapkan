<div id="step1">
    <div class="row">
        <form action="{{ route('panel.match.store2', $match['id']) }}" id="form2">
            {{ csrf_field() }}
        @for($i=1; $i<=2; $i++)
            <div class="col-md-6">
                <div class="card">
                    <div class="card-content">
                        <h4 align="center">{{ $match->getTeamName($i) }}</h4>
                        <div class="row form-horizontal">
                            <label class="col-sm-2 label-on-left">Счёт</label>
                            <div class="col-sm-10">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input type="text" class="form-control" name="team{{ $i }}_goals" style="text-align: center" value="{{ $match['team'.$i.'_goals'] }}">
                                    <span class="help-block">Введите сколько шайб забросила команда {{ $match->getTeamName($i) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endfor
        </form>
    </div>
    <script>
        function save_step2(completion) {
            console.log('save step 2');
            //

            $.ajax({
                type: 'post',
                url: $('#form2').attr('action'),
                data: $('#form2').serialize(),
                success: function () {

                }
            }).done(function (data) {
                console.log(data);
                alert('Счёт записан');
                completion();
            }).fail(function (data) {
                console.log(data);
                alert('Ошибка');
            });

        }
    </script>
</div>