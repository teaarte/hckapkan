@extends('layouts.panel')

@section('content')
    <script>
        //tinymce.init({ selector:'textarea' });
        tinymce.init({
            selector: 'textarea',
            height: 500,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    </script>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-icon" data-background-color="green">
                    <i class="material-icons">add_circle_outline</i>
                </div>
                <div class="card-content">
                    <?php
                    $route = route('panel.page.store');
                    if (isset($page)) {
                        $route = route('panel.page.update',$page['id']);
                    }
                    ?>
                    <h4 class="card-title">{{ $title }}</h4>
                    <form action="{{ $route }}" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input type="text" class="form-control" name="title" placeholder="Название" value="{{ $page['title'] or "" }}">
                                    <span class="help-block">Название</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <input type="text" class="form-control" name="slug" placeholder="Адрес" value="{{ $page['slug'] or "" }}">
                                    <span class="help-block">Адрес. Например site.ru/<b>about</b></span>
                                </div>
                            </div>
                        </div>

                        <br>

                        <textarea name="text" rows="12">{{ $page['text'] or "" }}</textarea>

                        <div align="center">



                            <br>
                            <br>

                            <input type="submit" class="btn btn-success" value="Сохранить" style="width: 70%">
                            <div class="" style="width: 200px;" align="left">
                                <div class="col-sm-12">
                                    <div class="checkbox checkbox-inline">
                                        <label>

                                            <?php
                                            $value = "";
                                            if (isset($page)) {
                                                if ($page['visible_menu'] == true)
                                                    $value = "checked";
                                            }
                                            ?>
                                            <input type="checkbox" name="visible_menu" {{ $value }}> Отображать
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection