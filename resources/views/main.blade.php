@extends('layouts.app')

@section('content')
    <!-- main box -->
    <div class="main-box cnt-section">
        <div class="inner-wrap">
            <div class="inner-block frm-dark">
                @include('components.main.last_news')
                @include('components.main.matches', ['title' => 'Последние матчи', 'matches' => $last_matches])
                @include('components.main.matches', ['title' => 'Ближайшие матчи', 'matches' => $feature_matches])
            </div>
        </div>
    </div>
    <!-- /main box -->
    <!-- page -->
    <div class="page">
        @include('components.main.best_players', $best_players)
        @include('components.main.tables', $tables)
        <!-- columns box -->
            <div class="columns-box">
                <div class="inner-block">
                    @include('components.main.media_column', $media_column)
                    @include('components.main.birthday_column', $birthday_column)
                </div>
            </div>
    </div>


@endsection