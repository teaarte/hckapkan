@extends('layouts.app')

@section('content')
    <script>
        $( document ).ready(function() {
            $('#team_select').on('selectmenuchange', function (event, ui) {
                window.location.href = "{{ route('team.index') }}/"+ui.item.value;
            });
        });
    </script>

    <!-- page -->
    <div class="page">
        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
            <div class="title">Вы находитесь здесь:</div>
            <div class="links">
                <a href="/">Главная</a>
                <span>Состав</span>
            </div>
        </div>
        <!-- /breadcrumbs -->
        <div class="title-wrap-block">
            <h1>СОСТАВ</h1>
            <div class="select-block w170">
                <select id="team_select">
                    <option value="0">Все команды</option>
                    @foreach($teams as $_team)
                        <option value="{{ $_team['id'] }}" @if($_team['id']==$team_id) selected @endif>{{ $_team['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        @foreach($position_names as $position_name)
            <div class="line-title-block">
                <h2>{{ $position_name['name'] }}</h2>
            </div>
            <!-- players box -->
            <div class="players-box">
            @foreach($players->where('position', $position_name['position']) as $player)
                <!-- item -->
                    <div class="item-wrap">
                        <a href="{{ route('player.show', $player['id']) }}" class="item-player">
                            <span class="photo-block"><img src="{{ $player->getImage() }}" alt=""></span>
                            <span class="name-block">{{ $player->getFio() }}</span>
                            @if ($player['number'])
                                <span class="frm-number">{{ $player['number'] }}</span>
                            @endif
                        </a>
                    </div>
                    <!-- /item -->
                @endforeach
            </div>
            <!-- /players box -->

        @endforeach



    </div>
    <!-- /page -->


@endsection