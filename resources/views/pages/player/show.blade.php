@extends('layouts.app')

@section('content')
    <!-- page -->
    <div class="page">
        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
            <div class="title">Вы находитесь здесь:</div>
            <div class="links">
                <a href="/">Главная</a>
                <span>Игроки</span>
            </div>
        </div>
        <!-- /breadcrumbs -->

        <!-- item one player -->
        <div class="item-one-player">
            <div class="photo-block">
                <img src="{{ $player->getImage() }}" alt="">
            </div>
            <div class="name-block main-title">{{ $player->getFio() }}</div>
            <div class="pos-block">{{ $player->getPositionName() }}</div>
            <div class="info-block">
                @if ($player['number'])
                    <div class="f-row">Игровой номер:
                        <div class="frm-number">{{ $player['number'] }}</div>
                    </div>
                @endif
                @if($player['height'] != null)
                    <div class="f-row">
                        Рост: <b>{{ $player['height'] }} см</b>
                    </div>
                @endif

                @if($player['weight'] != null)
                    <div class="f-row">
                        Вес: <b>{{ $player['weight'] }} кг</b>
                    </div>
                @endif

                @if($years != null)
                    <div class="f-row">
                        Возраст: <b>{{ $years }} лет ({{ $birthday->format('d.m.Y') }})</b>
                    </div>
                @endif

                @if ($player['url_vk'] || $player['url_fb'] || $player['url_ok'] || $player['url_spbhl'])
                    <div class="soc-block">
                        <div class="title">Социальные сети:</div>
                        <div class="buttons">
                            @if($player['url_vk'] != null)
                                <a href="{{ $player['url_vk'] }}" class="item-soc small soc01"></a>
                            @endif

                            @if($player['url_fb'] != null)
                                <a href="{{ $player['url_fb'] }}" class="item-soc small soc02"></a>
                            @endif

                            @if($player['url_ok'] != null)
                                <a href="{{ $player['url_ok'] }}" class="item-soc small soc05"></a>
                            @endif

                            @if($player['url_spbhl'] != null)
                                <a href="{{ $player['url_spbhl'] }}" class="item-soc small logo">
                                    <img src="/img/logo.png" alt="">
                                </a>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <!-- /item one player -->

        @if($player['position'] != 1)
            <h2 class="main-title">СТАТИСТИКА ИГРОКА</h2>
            <!-- stat box -->
            <div class="stat-box">
            @foreach($stats as $stat)
                <!-- item -->
                    <div class="item-wrap">
                        <div class="item-stat">
                            <table>
                                <col class="col01">
                                <col class="col02">
                                <thead>
                                <tr>
                                    <th colspan="2">{{ $stat['team'] }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Очки</td>
                                    <td>
                                        <b>{{ $stat['pass'] }}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Голы</td>
                                    <td>
                                        @if($stat['throw'] != 0)
                                            <b>{{ $stat['throw'] }}</b>
                                        @else
                                            0
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Передачи</td>
                                    <td>
                                        @if($stat['asist'] != 0)
                                            <b>{{ $stat['asist'] }}</b>
                                        @else
                                            0
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /item -->
                @endforeach
            </div>
        @endif

    </div>
    <!-- /page -->


@endsection