@extends('layouts.app')

@section('content')
    <!-- page -->
    <div class="page">
        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
            <div class="title">Вы находитесь здесь:</div>
            <div class="links">
                <a href="/">Главная</a>
                <span>Таблицы</span>
            </div>
        </div>
        <!-- /breadcrumbs -->

        <div class="title-wrap-block">
            <h1>Таблицы</h1>
        </div>


        @foreach($tables as $table)
            @if($table->getHeaderRow() != null)
                <h2 class="small-title">{{ $table['name'] }}</h2>
                <div class="tbl-tables">
                    <table class="">
                        <col class="col01">
                        <col class="col02">
                        <col class="col03">
                        <col class="col04">
                        <col class="col05">
                        <col class="col06">
                        <col class="col07">
                        <col class="col07">
                        <col class="col08">
                        <thead>
                        <tr>
                            <?php
                            $header_row = $table->getHeaderRow();
                            ?>
                            @for($i = 0; $i <= 8; $i++)
                                <th>
                                    {{ $header_row->getString($i) }}
                                </th>
                            @endfor
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i_row = 1;
                        $rows = $table->getBodyRows();
                        $team_name = $table->getTeamName();
                        ?>
                        @foreach($rows as $row)
                            <tr>
                                @for($i = 0; $i <= 8; $i++)
                                    <td>
                                        @if (($team_name != "" && strpos($row->getString(1), $team_name) !== false) || $i == 8)
                                            <b>{{ $row->getString($i) }}</b>
                                        @else
                                            {{ $row->getString($i) }}
                                        @endif
                                    </td>
                                @endfor
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        @endforeach

    </div>
    <!-- /page -->


@endsection