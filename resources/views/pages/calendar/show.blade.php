@extends('layouts.app')

@section('content')

    <script>
        $( document ).ready(function() {
            $('#team_select').on('selectmenuchange', function (event, ui) {
                var calendar_id = $("#calendar_select").val();
                var team_id = ui.item.value;
                window.location.href = "{{ route('calendar.index') }}/"+calendar_id+'-'+team_id;
            });
            $('#calendar_select').on('selectmenuchange', function (event, ui) {
                var calendar_id = ui.item.value;
                var team_id = $("#team_select").val();
                window.location.href = "{{ route('calendar.index') }}/"+calendar_id+'-'+team_id;
            });
        });
    </script>

    <!-- page -->
    <div class="page">

        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
            <div class="title">Вы находитесь здесь:</div>
            <div class="links">
                <a href="/">Главная</a>
                <span>Новости</span>
            </div>
        </div>
        <!-- /breadcrumbs -->

        <div class="title-wrap-block">
            <h1>КАЛЕНДАРЬ</h1>
            <div class="select-block w310">
                <select id="calendar_select">
                    <option value="0">Все</option>
                    @foreach($calendars as $calendar)
                        <option value="{{ $calendar['id'] }}" @if($calendar['id']==$calendar_id) selected @endif>{{ $calendar['name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="select-block w170">
                <select id="team_select">
                    <option value="0">Все</option>
                    @foreach($teams as $team)
                        <option value="{{ $team['id'] }}" @if($team['id']==$team_id) selected @endif>{{ $team['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="games-box">
            @foreach($matches as $match)
                <!-- item -->
                    <div class="item-wrap">
                        <div class="item-game">
                            <a @if($match['finished'] == 1) href="{{ route('match.show', $match['id']) }}" @endif  class="link"></a>
                            <div class="item-total">
                                <div class="team-block @if($match->checkWinner(1)) winner @endif">
                                    <div class="name">{{ $match->getTeamName(1) }}</div>
                                    <div class="logo">
                                        <img src="{{ $match->getTeamLogo(1) }}" alt="">
                                    </div>
                                </div>
                                <div class="team-block  @if($match->checkWinner(2)) winner @endif">
                                    <div class="name">{{ $match->getTeamName(2) }}</div>
                                    <div class="logo">
                                        <img src="{{ $match->getTeamLogo(2) }}" alt="">
                                    </div>
                                </div>
                            </div>
                            @if ($match['finished'] == 1)
                                <div class="score">{{ $match['team1_goals'] }}:{{ $match['team2_goals'] }}</div>
                            @endif
                            <div class="game-row">
                                {{ $match->getDate() }} в {{ $match->getTime() }}
                            </div>
                        </div>
                    </div>
                    <!-- /item -->
            @endforeach
        </div>



        <div class="tbl-games">
            <div class="tbl-row-title">
                <a href="" class="link"></a>
                <table>
                    <col class="col01">
                    <col class="col02">
                    <col class="col03">
                    <col class="col04">
                    <col class="col05">
                    <thead>
                    <tr>
                        <th>Дата игры</th>
                        <th>Время</th>
                        <th>Команды</th>
                        <th>Счет</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>

            @foreach($matches as $match)
                <div class="tbl-row">
                    <a @if($match['finished'] == 1) href="{{ route('match.show', $match['id']) }}" @endif class="link"></a>
                    <table>
                        <col class="col01">
                        <col class="col02">
                        <col class="col03">
                        <col class="col04">
                        <col class="col05">
                        <tbody>
                        <tr>
                            <td>{{ $match->getDate() }}</td>
                            <td>{{ $match->getTime() }}</td>
                            <td>
                                <div class="item-total">
                                    <div class="team-block">
                                        <div class="name @if($match->checkWinner(1)) winner @endif">{{ $match->getTeamName(1) }}</div>
                                        <div class="logo">
                                            <img src="{{ $match->getTeamLogo(1) }}" alt="">
                                        </div>
                                    </div>
                                    <div class="team-block @if($match->checkWinner(2)) winner @endif">
                                        <div class="name">{{ $match->getTeamName(2) }}</div>
                                        <div class="logo">
                                            <img src="{{ $match->getTeamLogo(2) }}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                @if ($match['finished'] == 1)
                                    <div class="score">{{ $match['team1_goals'] }}:{{ $match['team2_goals'] }}</div>
                                @endif
                            </td>
                            <td>
                                @if ($match['finished'] == 1)
                                    <a href="" class="btn-stat"></a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            @endforeach


        </div>



    </div>
    <!-- /page -->

@endsection