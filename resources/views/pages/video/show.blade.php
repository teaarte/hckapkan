@extends('layouts.app')

@section('content')
    <!-- page -->
    <div class="page">

        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
            <div class="title">Вы находитесь здесь:</div>
            <div class="links">
                <a href="/">Главная</a>
                <span>Видео</span>
            </div>
        </div>
        <!-- /breadcrumbs -->

        <!-- cnt info block -->
        <div class="cnt-info-box">
            <div class="frm-date">{{ $video->getWordDate() }}</div>
            <div class="link"><a href="{{ route('video.index') }}">Все видео</a>
            </div>
        </div>
        <!-- /cnt info block -->

        <h1>{{ $video->getTitle() }}</h1>


        <iframe style="width: 100%; height: 40vw" src="{{ $video->getIframeUrl() }}" frameborder="0" allowfullscreen></iframe>

    <!-- cnt info block -->
        <div class="cnt-info-box bottom">
            <div class="soc-block">
                <div class="title">Поделиться</div>
                <div class="social-likes buttons">
                    <a href="#" class="vkontakte item-soc soc01" title="Поделиться ссылкой во Вконтакте"></a>
                    <a href="#" class="facebook item-soc soc02" title="Поделиться ссылкой на Фейсбуке"></a>
                    <a href="#" class="twitter item-soc soc03" title="Поделиться ссылкой в Твиттере"></a>
                    <a href="#" class="plusone item-soc soc04" title="Поделиться ссылкой в Гугл-плюсе"></a>
                    <a href="#" class="odnoklassniki item-soc soc05" title="Поделиться ссылкой в Одноклассниках"></a>
                </div>
            </div>
            <div class="link">
                <a href="{{ route('video.index') }}">Все видео</a>
            </div>
        </div>
        <!-- /cnt info block -->

    </div>
    <!-- /page -->


@endsection