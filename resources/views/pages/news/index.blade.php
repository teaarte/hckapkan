@extends('layouts.app')

@section('content')
    <!-- page -->
    <div class="page">
        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
            <div class="title">Вы находитесь здесь:</div>
            <div class="links">
                <a href="/">Главная</a>
                <span>Новости</span>
            </div>
        </div>
        <!-- /breadcrumbs -->
        <h1>Новости</h1>

        <!-- news box -->
        <div class="news-box">
            @include('pages.news.news', ['news' => $news, 'page' => 2])
        </div>
        <!-- /news box -->

        <script>
            $(window).on('hashchange', function() {
                if (window.location.hash) {
                    var page = window.location.hash.replace('#', '');
                    if (page == Number.NaN || page <= 0) {
                        return false;
                    } else {
                        getPosts(page);
                    }
                }
            });
            $(document).ready(function() {
                $(document).on('click', '.btn-loader', function (e) {
                    getPosts($(this).attr('href').split('page=')[1]);
                    e.preventDefault();
                });
            });
            function getPosts(page) {
                console.log(page);
                $.ajax({
                    url : '?get_page=' + page,
                }).done(function (data) {
                    console.log(data);
                    $('.news-box').html(data);
                    location.hash = page;
                }).fail(function () {
                    alert('Posts could not be loaded.');
                });
            }
        </script>
    </div>
    <!-- /page -->


@endsection