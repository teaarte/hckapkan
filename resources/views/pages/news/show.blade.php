@extends('layouts.app')

@section('content')
    <!-- page -->
    <div class="page">

        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
            <div class="title">Вы находитесь здесь:</div>
            <div class="links">
                <a href="/">Главная</a>
                <span>Новости</span>
            </div>
        </div>
        <!-- /breadcrumbs -->

        <!-- cnt info block -->
        <div class="cnt-info-box">
            <div class="frm-date">{{ $news->getWordDate() }}</div>
            <div class="link"><a href="{{ route('news.index') }}">Все новости</a>
            </div>
        </div>
        <!-- /cnt info block -->

        <h1>{{ $news->getTitle() }}</h1>
        {!! $news->getText() !!}

        @if($news->getImages()->count()>0)
            <!-- gallery box -->
            <div class="gallery-box">
                <?php $i = 0; ?>
                @foreach($news->getImages() as $image)
                    <!-- item wrap -->
                    <div class="item-wrap">
                        <a href="" class="item-gallery" data-gal="{{ $i }}"><span class="inner-block"><img src="{{ $image }}" alt=""></span></a>
                    </div>
                    <!-- /item wrap -->
                    <?php $i++; ?>
                @endforeach

            </div>
            <!-- /gallery box -->
        @endif

        <!-- cnt info block -->
        <div class="cnt-info-box bottom">
            <div class="soc-block">
                <div class="title">Поделиться</div>
                <div class="social-likes buttons">
                    <a href="#" class="vkontakte item-soc soc01" title="Поделиться ссылкой во Вконтакте"></a>
                    <a href="#" class="facebook item-soc soc02" title="Поделиться ссылкой на Фейсбуке"></a>
                    <a href="#" class="twitter item-soc soc03" title="Поделиться ссылкой в Твиттере"></a>
                    <a href="#" class="plusone item-soc soc04" title="Поделиться ссылкой в Гугл-плюсе"></a>
                    <a href="#" class="odnoklassniki item-soc soc05" title="Поделиться ссылкой в Одноклассниках"></a>
                </div>
            </div>
            <div class="link">
                <a href="{{ route('news.index') }}">Все новости</a>
            </div>
        </div>
        <!-- /cnt info block -->

    </div>
    <!-- /page -->

    @if($news->getImages()->count()>0)
        <div class="popup-box">
            <div class="gallery-slider-box">
                <div class="wrap-block">
                    <div class="inner-wrap">
                        <a href="" class="btn-close">Закрыть</a>
                        <div class="main-slider">
                            <div class="slider">
                                <?php $i = 0; ?>
                                @foreach($news->getImages() as $image)
                                    <div class="sl-item" data-slide="{{ $i }}">
                                        <img src="{{ $image }}" alt="">
                                    </div>
                                    <?php $i++; ?>
                                @endforeach
                            </div>
                        </div>
                        <div class="preview-slider">
                            <div class="slider">
                                <?php $i = 0; ?>
                                @foreach($news->getImages() as $image)
                                    <div class="sl-item" data-slide="{{ $i }}">
                                        <div class="photo-block">
                                            <img src="{{ $image }}" alt="">
                                        </div>
                                    </div>
                                    <?php $i++; ?>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif




@endsection