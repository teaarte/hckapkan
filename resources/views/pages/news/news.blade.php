@foreach($news as $new)
    <!-- item -->
    <div class="item-wrap">
        <div class="item-new">
            <div class="frm-date">{{ $new->getDate() }}</div>
            <h2><a href="{{ $new->getUrl() }}">{{ $new->getTitle() }}</a></h2>
            @if($new->getPhoto() != "")
                <div class="photo-block">
                    <img src="{{ $new->getPhoto() }}" alt="{{ $new->getTitle() }}">
                </div>
            @endif
            <p>{{ $new->getShortText() }}</p>
        </div>
    </div>
    <!-- /item -->
@endforeach

@if($paginator)
    <!-- load box -->
    <div class="load-box">
        <a href="?page={{ $page }}" class="btn btn-secondary btn-loader">Показать еще новости</a>
    </div>
    <!-- /load box -->
@endif