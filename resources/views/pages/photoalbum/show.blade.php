@extends('layouts.app')

@section('content')
    <!-- page -->
    <div class="page">

        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
            <div class="title">Вы находитесь здесь:</div>
            <div class="links">
                <a href="/">Главная</a>
                <a href="{{ route('photoalbum.index') }}">Фотоальбомы</a>
                <span>{{ $photoalbum['name'] }}</span>
            </div>
        </div>
        <!-- /breadcrumbs -->


        <h1>{{ $photoalbum['name'] }}</h1>



        <!-- gallery box -->
            <div class="gallery-box">
            <?php $i = 0; ?>
            @foreach($photoalbum->photos()->get() as $photo)
                <!-- item wrap -->
                    <div class="item-wrap">
                        <a href="" class="item-gallery" data-gal="{{ str_pad($i, 3, '0', STR_PAD_LEFT) }}"><span class="inner-block"><img src="{{ $photo['url'] }}" alt=""></span></a>
                    </div>
                    <!-- /item wrap -->
                    <?php $i++; ?>
                @endforeach

            </div>
            <!-- /gallery box -->



    </div>
    <!-- /page -->

    <div class="popup-box">
        <div class="gallery-slider-box">
            <div class="wrap-block">
                <div class="inner-wrap">
                    <a href="" class="btn-close">Закрыть</a>
                    <div class="main-slider">
                        <div class="slider">
                            <?php $i = 0; ?>
                            @foreach($photoalbum->photos()->get() as $photo)
                                <div class="sl-item" data-slide="{{ str_pad($i, 3, '0', STR_PAD_LEFT) }}">
                                    <img src="{{ $photo['url'] }}" alt="">
                                </div>
                                <?php $i++; ?>
                            @endforeach
                        </div>
                    </div>
                    <div class="preview-slider">
                        <div class="slider">
                            <?php $i = 0; ?>
                            @foreach($photoalbum->photos()->get() as $photo)
                                <div class="sl-item" data-slide="{{ str_pad($i, 3, '0', STR_PAD_LEFT) }}">
                                    <div class="photo-block">
                                        <img src="{{ $photo['url'] }}" alt="">
                                    </div>
                                </div>
                                <?php $i++; ?>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection