@extends('layouts.app')

@section('content')
    <!-- page -->
    <div class="page">
        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
            <div class="title">Вы находитесь здесь:</div>
            <div class="links">
                <a href="/">Главная</a>
                <span>Медиа</span>
            </div>
        </div>
        <!-- /breadcrumbs -->

        <h1>
            @if($type=="photo")
                ФОТОГАЛЕРЕЯ
            @else
                ВИДЕОГАЛЕРЕЯ
            @endif
        </h1>

        <!-- media gallery box -->
        <div class="media-gallery-box">
            @foreach($photoalbums as $photoalbum)
                @if($type != "photo" || $photoalbum->getPhotosCount() > 0)
                    <!-- item -->
                    <div class="item-wrap">
                        <a
                            @if($type=="photo")
                                href="{{ route('photoalbum.show', $photoalbum['id']) }}"
                            @else
                                href="{{$photoalbum->getLink()}}"
                            @endif
                                class="item-mgallery">
                                <span class="@if($type=="photo") photo-block @else video-block @endif">
                                    <img src="{{ $photoalbum->getThumbail() }}" alt="">
                                    @if($type=="photo")
                                        <span class="frm-count">{{ $photoalbum->getPhotosCount() }}</span>
                                    @endif
                                </span>
                            <span class="title-block">{{ $photoalbum['name'] }}</span>
                            <span class="frm-date">{{ $photoalbum->getDate() }}</span>
                        </a>
                    </div>
                    <!-- /item -->
                @endif
            @endforeach

        </div>
        <!-- /media gallery box -->

    </div>
    <!-- /page -->

    <div class="empty"></div>
@endsection