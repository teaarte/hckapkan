@extends('layouts.app')

@section('content')
    <!-- page -->
    <div class="page">

        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
        </div>
        <!-- /breadcrumbs -->


        <h1>{{ $page->getTitle() }}</h1>

        {!! $page->getText() !!}

        <!-- cnt info block -->
        <div class="cnt-info-box bottom">
            <div class="soc-block">
                <div class="title">Поделиться</div>
                <div class="social-likes buttons">
                    <a href="#" class="vkontakte item-soc soc01" title="Поделиться ссылкой во Вконтакте"></a>
                    <a href="#" class="facebook item-soc soc02" title="Поделиться ссылкой на Фейсбуке"></a>
                    <a href="#" class="twitter item-soc soc03" title="Поделиться ссылкой в Твиттере"></a>
                    <a href="#" class="plusone item-soc soc04" title="Поделиться ссылкой в Гугл-плюсе"></a>
                    <a href="#" class="odnoklassniki item-soc soc05" title="Поделиться ссылкой в Одноклассниках"></a>
                </div>
            </div>
        </div>
        <!-- /cnt info block -->

    </div>
    <!-- /page -->



@endsection