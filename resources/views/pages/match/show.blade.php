@extends('layouts.app')

@section('content')

    <!-- page -->
    <div class="page">

        <!-- breadcrumbs -->
        <div class="breadcrumbs-box">
            <div class="title">Вы находитесь здесь:</div>
            <div class="links">
                <a href="/">Главная</a>
                <span>Матч</span>
            </div>
        </div>
        <!-- /breadcrumbs -->
    @if($match['finished'] == 1)
        <!-- result box -->
            <div class="result-box">
                <div class="team-block">
                    <div class="logo-block">
                        <img src="{{ $match->getTeamLogo(1) }}" alt="">
                    </div>
                    <div class="name-block">{{ $match->getTeamName(1) }}</div>
                </div>
                <div class="result-block">
                    <div class="date-block">{{ $match->getDate() }}</div>
                    <div class="location-block">{{ $match->getArenaName() }}</div>
                    <div class="score-block">
                        <div class="score">{{ $match['team1_goals'] }}</div>
                        <div class="score">{{ $match['team2_goals'] }}</div>
                    </div>
                    <div class="period-block">
                        @foreach($period_results as $period_result)
                            <div class="period">{{ $period_result[0] }}:{{ $period_result[1] }}</div>
                        @endforeach
                    </div>
                </div>
                <div class="team-block">
                    <div class="logo-block">
                        <img src="{{ $match->getTeamLogo(2) }}" alt="">
                    </div>
                    <div class="name-block">{{ $match->getTeamName(2) }}</div>
                </div>
            </div>
            <!-- /result box -->


            <!-- goals result box -->
            <div class="goals-result-box">
                <h2 class="main-title center">ГОЛЫ</h2>
            <?php $period = 1; ?>
            @foreach($goals_results as $goals_result)
                <!-- period -->
                    <div class="period-title">{{ $period }} период</div>
                    <div class="items-wrap">
                    @foreach($goals_result as $goal)
                        <!-- item -->
                            <div class="goal-block @if($goal['team'] == 0) left @else right @endif">
                                <div class="players-info">
                                    <div class="name main">
                                        @if($goal['throw_link'] != '')
                                            <a href="{{ $goal['throw_link'] }}">{{ $goal['throw'] }}</a>
                                        @else
                                            {{ $goal['throw'] }}
                                        @endif
                                    </div>
                                    <div class="name">
                                        @if($goal['asist1_link'] != '')
                                            <a href="{{ $goal['asist1_link'] }}">{{ $goal['asist1'] }}</a>
                                        @else
                                            {{ $goal['asist1'] }}
                                        @endif
                                    </div>
                                    <div class="name">
                                        @if($goal['asist2_link'] != '')
                                            <a href="{{ $goal['asist2_link'] }}">{{ $goal['asist2'] }}</a>
                                        @else
                                            {{ $goal['asist2'] }}
                                        @endif
                                    </div>
                                </div>
                                <div class="goal-info">
                                    <div class="score">{{ $goal['common_results'][0] }}:{{ $goal['common_results'][1] }}</div>
                                    <div class="time">{{ $goal['time'] }}</div>
                                </div>
                            </div>
                            <!-- /item -->
                        @endforeach
                    </div>
                    <!-- /period -->
                    <?php $period ++; ?>
                @endforeach
            </div>
            <!-- /goals result box -->

            <!-- composition box -->
            <div class="composition-box">
                <h2 class="main-title center">Состав</h2>
            @foreach($position_names as $position_name)
                <!-- composition block -->
                    <div class="line-title-block one">
                        <h3>{{ $position_name['name'] }}</h3>
                    </div>
                    <div class="items-wrap">
                    @foreach($matchteam->players()->get()->where('in_team', 1)->where('position', $position_name['position']) as $player)
                        <!-- item -->
                            <div class="item-wrap">
                                <a href="{{ $player->getLink() }}" class="item-player-small">
                            <span class="photo-block">
                                <img src="{{ $player->getImage() }}" alt="">
                            </span>
                                    @if($player['number'] != 0)
                                        <span class="frm-number small">{{ $player['number'] }}</span>
                                    @endif
                                    {{ $player->getNameSurname() }}
                                </a>
                            </div>
                            <!-- /item -->
                        @endforeach

                    </div>
                    <!-- /composition block -->
                @endforeach

            </div>
            <!-- /composition box -->


        @if($photo != null || $video != null)
            <!-- media wrap box -->
                <div class="media-wrap-box">
                    @if($photo != null)
                        @if($photo->getPhotosCount() > 0)
                            <div class="item-wrap">
                                <h2 class="main-title center">Фото</h2>
                                <a href="{{ route('photoalbum.show', $photo['id']) }}" class="item-media">
                                    <span class="photo-block"><img src="{{ $photo->getThumbail() }}" alt=""></span>
                                    <span class="frm-count">{{ $photo->getPhotosCount() }}</span>
                                </a>
                            </div>
                        @endif
                    @endif
                    @if($video != null)
                        <div class="item-wrap">
                            <h2 class="main-title center">Видео</h2>
                            <a href="{{ $video->getLink() }}" class="item-media">
                                <span class="video-block main"><img src="{{ $video->getThumbail() }}" alt=""></span>
                            </a>
                        </div>
                    @endif
                </div>
                <!-- /media wrap box -->
        @endif

        <!-- cnt info block -->
            <div class="cnt-info-box bottom">
                <div class="soc-block center">
                    <div class="title">Поделиться</div>
                    <div class="social-likes buttons">
                        <a href="#" class="vkontakte item-soc soc01" title="Поделиться ссылкой во Вконтакте"></a>
                        <a href="#" class="facebook item-soc soc02" title="Поделиться ссылкой на Фейсбуке"></a>
                        <a href="#" class="twitter item-soc soc03" title="Поделиться ссылкой в Твиттере"></a>
                        <a href="#" class="plusone item-soc soc04" title="Поделиться ссылкой в Гугл-плюсе"></a>
                        <a href="#" class="odnoklassniki item-soc soc05" title="Поделиться ссылкой в Одноклассниках"></a>
                    </div>
                </div>
            </div>
            <!-- /cnt info block -->
        @else
            <h2 align="center">Матч ещё не завершён</h2>
        @endif
    </div>
    <!-- /page -->

@endsection