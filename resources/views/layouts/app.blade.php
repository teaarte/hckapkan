<!DOCTYPE html>
<html lang="ru">

<head>
    <title>{{ $title or config('app.name', 'Хоккейный клуб «Капкан»') }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8" />
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/reset.css" />
    <link rel="stylesheet" href="../css/style.css" />
    <link rel="stylesheet" href="../css/jquery-ui.min.css" />
    <link rel="stylesheet" href="../css/owl.carousel.css" />
    <!--[if lte IE 8]><link href= "../css/ie.css" rel= "stylesheet" media= "all" /><![endif]-->
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.min.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/jquery.placeholder.min.js"></script>
    <script src="../js/scripts.js"></script>
    <script src="../js/social-likes.js"></script>
    <link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">
    <link rel="shortcut icon" href="favicon.ico">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

</head>
<body>
    <!-- wrap -->
    <div class="wrap">
        @include('components.header')
        @yield('content')

        <div class="empty"></div>
    </div>
    <!-- /wrap -->

    @include('components.footer')
</body>

</html>