<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="/assets_panel/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/assets_panel/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{{ $title or 'Админ-панель - Хоккейный клуб «Капкан»' }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="/assets_panel/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="/assets_panel/css/material-dashboard.css" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="/assets_panel/css/demo.css" rel="stylesheet" />
    <link href="/css/dropzone.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Scripts -->

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=4pm1oc1cpgbi9g65fkgqia34vpbq2tq58g9tjslrb883xo9n"></script>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!--   Core JS Files   -->
    <script src="/assets_panel/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="/assets_panel/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/assets_panel/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets_panel/js/material.min.js" type="text/javascript"></script>
    <script src="/assets_panel/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <!-- Forms Validations Plugin -->


</head>
<body>
<div class="wrapper">
    <div class="sidebar" data-active-color="red" data-background-color="black" data-image="/assets_panel/img/sidebar-1.jpg">
        <!--
    Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
    Tip 2: you can also add an image using data-image tag
    Tip 3: you can change the color of the sidebar with data-background-color="white | black"
-->
        <div class="logo">
            <a href="/" class="simple-text">
                ХК "Капкан"
            </a>
        </div>
        <div class="logo logo-mini">
            <a href="/" href="" class="simple-text">
                ХК
            </a>
        </div>
        <div class="sidebar-wrapper">

            <ul class="nav">
                <li @if ($sidebar_group == "main") class="active" @endif >
                    <a href="{{ route('panel.main') }}">
                        <i class="material-icons">dashboard</i>
                        <p>Главная</p>
                    </a>
                </li>

                <li @if ($sidebar_group == "news") class="active" @endif>
                    <a href="{{ route('panel.news') }}">
                        <i class="material-icons">chrome_reader_mode</i>
                        <p>Новости</p>
                    </a>
                </li>

                <li @if ($sidebar_group == "table") class="active" @endif>
                    <a href="{{ route('panel.table') }}">
                        <i class="material-icons">grid_on</i>
                        <p>Таблицы</p>
                    </a>
                </li>

                <li @if ($sidebar_group == "player") class="active" @endif >
                    <a href="{{ route('panel.player') }}">
                        <i class="material-icons">person</i>
                        <p>Игроки</p>
                    </a>
                </li>

                <li @if ($sidebar_group == "team") class="active" @endif>
                    <a data-toggle="collapse" href="#team">
                        <i class="material-icons">group</i>
                        <p>Команды
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse  @if ($sidebar_group == "team") in @endif" id="team">
                        <ul class="nav">
                            <li @if ($sidebar_element == "our") class="active" @endif>
                                <a href="{{ route('panel.team.our') }}">Наши</a>
                            </li>
                            <li @if ($sidebar_element == "enemy") class="active" @endif>
                                <a href="{{ route('panel.team.enemy') }}">Соперники</a>
                            </li>
                        </ul>
                    </div>
                </li>


                <li @if ($sidebar_group == "arena") class="active" @endif>
                    <a href="{{ route('panel.arena') }}">
                        <i class="material-icons">add_location</i>
                        <p>Арены</p>
                    </a>
                </li>

                <li @if ($sidebar_group == "calendar") class="active" @endif>
                    <a href="{{ route('panel.calendar') }}">
                        <i class="material-icons">date_range</i>
                        <p>Календари</p>
                    </a>
                </li>

                <li @if ($sidebar_group == "match") class="active" @endif>
                    <a href="{{ route('panel.match') }}">
                        <i class="material-icons">monetization_on</i>
                        <p>Матчи</p>
                    </a>
                </li>


                <li @if ($sidebar_group == "photoalbum") class="active" @endif>
                    <a href="{{ route('panel.photoalbum') }}">
                        <i class="material-icons">photo_library</i>
                        <p>Фото</p>
                    </a>
                </li>


                <li @if ($sidebar_group == "video") class="active" @endif>
                    <a href="{{ route('panel.video') }}">
                        <i class="material-icons">video_library</i>
                        <p>Видео</p>
                    </a>
                </li>

                <li @if ($sidebar_group == "page") class="active" @endif>
                    <a href="{{ route('panel.page') }}">
                        <i class="material-icons">chrome_reader_mode</i>
                        <p>Страницы</p>
                    </a>
                </li>


            </ul>
        </div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                        <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                        <i class="material-icons visible-on-sidebar-mini">view_list</i>
                    </button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> {{ $title or "Админ-панель" }} </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">exit_to_app</i>
                                <p class="hidden-lg hidden-md">Выйти</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="/">
                                Главная
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Помощь
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">

                </p>
            </div>
        </footer>
    </div>
</div>
</body>

<script src="/assets_panel/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="/assets_panel/js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="/assets_panel/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="/assets_panel/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="/assets_panel/js/bootstrap-notify.js"></script>
<!-- DateTimePicker Plugin -->
<script src="/assets_panel/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="/assets_panel/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="/assets_panel/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<!-- Select Plugin -->
<script src="/assets_panel/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="/assets_panel/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="/assets_panel/js/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="/assets_panel/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="/assets_panel/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="/assets_panel/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="/assets_panel/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="/assets_panel/js/demo.js"></script>
<script src="/js/dropzone.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        //md.initSliders()
        demo.initFormExtendedDatetimepickers();
    });
</script>
</html>
