$(document).ready(function(){





    //placeholder
	$('input, textarea').placeholder();


	//select
	$('select').selectmenu();
	
	//popup block
	$('.popup-wrap .btn-toggle').on('click touchstart', function() {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active')
		} else {
			$('.popup-wrap:not(.no-close) .btn-toggle').removeClass('active');
			$(this).addClass('active');
		}
		return false;
	})
	$(document).click(function(event) {
	    if ($(event.target).closest(".popup-block").length) return;
	    $('.popup-wrap:not(.no-close) .btn-toggle').removeClass('active');
	    event.stopPropagation();
	});


	//main menu
	$('.main-menu-wrap li a').on('click', function() {
		if ($(this).next('ul').length>0) {
			$(this).parent('li').toggleClass('open');
			if ($(window).innerWidth()<768) {
				return false;
			}
		}
	})


	//load button
	/*
	$('.load-box .btn').on('click', function() {
		$(this).parent().addClass('active');
		return false;
	})*/

	//load tables
	$('.tr-link a').on('click', function() {
        $(this).parents('.tr-link').remove();
		return false;
	})


	//gallery slider box
	var owlMain=$('.gallery-slider-box .main-slider .slider').owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		dots: false,
		smartSpeed: 300,
		fluidSpeed: 300,
		autoplaySpeed: 300,
		navSpeed: 300,
		dotsSpeed: 300,
		dragEndSpeed: 300,
		responsiveRefreshRate: 100,
		onTranslated: function() {
			var currentSlide=$('.gallery-slider-box .main-slider .owl-item.active .sl-item').attr('data-slide');
			$('.gallery-slider-box .preview-slider .sl-item.active').removeClass('active');
			$('.gallery-slider-box .preview-slider .sl-item[data-slide="'+currentSlide+'"]').addClass('active');
			owlPreview.trigger('to.owl.carousel', [currentSlide,300,true]);
		}
	})
	var owlPreview=$('.gallery-slider-box .preview-slider .slider').owlCarousel({
		loop: false,
		nav: false,
		dots: false,
		smartSpeed: 1000,
		fluidSpeed: 1000,
		autoplaySpeed: 1000,
		navSpeed: 1000,
		dotsSpeed: 1000,
		dragEndSpeed: 1000,
		responsiveRefreshRate: 100,
		responsive: {
			0: {items: 2},
			480: {items: 3},
			640: {items: 4},
			768: {items: 5}
		},
	})

	$('.gallery-slider-box .preview-slider .sl-item').on('click', function() {
			$('.gallery-slider-box .preview-slider .sl-item.active').removeClass('active');
			$(this).addClass('active');
			var currentSetSlide=$('.gallery-slider-box .preview-slider .sl-item.active').attr('data-slide');
			owlMain.trigger('to.owl.carousel', [currentSetSlide,300,true]);
			var currentSlide=$('.gallery-slider-box .main-slider .owl-item.active .sl-item').attr('data-slide');
			$('.gallery-slider-box .preview-slider .sl-item[data-slide="'+currentSlide+'"]').addClass('active');

			return false;
	})
	var currentSlide=$('.gallery-slider-box .main-slider .owl-item.active .sl-item').attr('data-slide');
	$('.gallery-slider-box .preview-slider .sl-item[data-slide="'+currentSlide+'"]').addClass('active');
	$('[data-gal]').on('click', function() {
		$('.wrap').addClass('gallery-show');
		$('.page').css('min-height', $('.gallery-slider-box').height()+50);
        console.log(currentSlide);
        console.log($(this).attr('data-gal'));
        $('.gallery-slider-box .preview-slider .sl-item[data-slide="'+$(this).attr('data-gal')+'"]').addClass('active');
        owlMain.trigger('to.owl.carousel', [$(this).attr('data-gal'),1,true]);
        return false;
	})
	$('.popup-box .btn-close').on('click', function() {
		$('.wrap').removeClass('gallery-show');
		$('.page').css('min-height', 0);
		return false;
	})


    $(document).click(function(event) {
        if ($(event.target).closest(".popup-box .inner-wrap").length) return;
        $('.wrap').removeClass('gallery-show')
        event.stopPropagation();
    });
	

});
$(window).resize(function() {
      if ($('.wrap').hasClass('gallery-show')) {
      	$('.page').css('min-height', $('.gallery-slider-box').height()+50);
      } else {
      	$('.page').css('min-height', 0);
      }
});