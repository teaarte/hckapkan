<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'app.main', 'uses' => 'MainController@index']);

Auth::routes();

Route::group(['prefix' => '/'], function () {
    \Illuminate\Support\Facades\Session::flush();
    Route::get('/home', 'HomeController@index');


    Route::group(['prefix' => 'news'], function () {
        Route::get('/', ['as' => 'news.index', 'uses' => 'NewsController@index']);
        Route::get('/{id}-{title}', ['as' => 'news.show', 'uses' => 'NewsController@show']);
    });

    Route::group(['prefix' => 'player'], function () {
        Route::get('/', ['as' => 'player.index', 'uses' => 'PlayerController@index']);
        Route::get('/{id}', ['as' => 'player.show', 'uses' => 'PlayerController@show']);
    });

    Route::group(['prefix' => 'team'], function () {
        Route::get('/', ['as' => 'team.index', 'uses' => 'TeamController@index']);
        Route::get('/{id}', ['as' => 'team.show', 'uses' => 'TeamController@show']);
    });

    Route::group(['prefix' => 'calendar'], function () {
        Route::get('/', ['as' => 'calendar.index', 'uses' => 'CalendarController@index']);
        Route::get('/{calendar_id}-{team_id}', ['as' => 'calendar.show', 'uses' => 'CalendarController@show']);
    });

    Route::group(['prefix' => 'table'], function () {
        Route::get('/', ['as' => 'table.index', 'uses' => 'TableController@index']);
    });

    Route::group(['prefix' => 'match'], function () {
        Route::get('/{id}', ['as' => 'match.show', 'uses' => 'MatchController@show']);
    });

    Route::group(['prefix' => 'photoalbum'], function () {
        Route::get('/', ['as' => 'photoalbum.index', 'uses' => 'PhotoalbumController@index']);
        Route::get('/{id}', ['as' => 'photoalbum.show', 'uses' => 'PhotoalbumController@show']);
    });

    Route::group(['prefix' => 'video'], function () {
        Route::get('/', ['as' => 'video.index', 'uses' => 'PhotoalbumController@index_video']);
        Route::get('/{id}', ['as' => 'video.show', 'uses' => 'VideoController@show']);
    });

});

Route::group(['prefix' => 'panel'], function () {
    Route::get('/', ['as' => 'panel.main', 'uses' => 'Panel\PanelController@index']);
    Route::group(['prefix' => 'news'], function () {
        Route::get('/', ['as' => 'panel.news', 'uses' => 'Panel\PanelNewsController@index']);
        Route::post('/', ['as' => 'panel.news.store', 'uses' => 'Panel\PanelNewsController@store']);
        Route::get('/create', ['as' => 'panel.news.create', 'uses' => 'Panel\PanelNewsController@create']);
        Route::get('/edit/{id}', ['as' => 'panel.news.edit', 'uses' => 'Panel\PanelNewsController@edit']);
        Route::get('/delete/{id}', ['as' => 'panel.news.destroy', 'uses' => 'Panel\PanelNewsController@destroy']);
        Route::post('/edit/{id}', ['as' => 'panel.news.update', 'uses' => 'Panel\PanelNewsController@update']);
    });

    Route::group(['prefix' => 'page'], function () {
        Route::get('/', ['as' => 'panel.page', 'uses' => 'Panel\PanelPagesController@index']);
        Route::post('/', ['as' => 'panel.page.store', 'uses' => 'Panel\PanelPagesController@store']);
        Route::get('/create', ['as' => 'panel.page.create', 'uses' => 'Panel\PanelPagesController@create']);
        Route::get('/edit/{id}', ['as' => 'panel.page.edit', 'uses' => 'Panel\PanelPagesController@edit']);
        Route::get('/delete/{id}', ['as' => 'panel.page.destroy', 'uses' => 'Panel\PanelPagesController@destroy']);
        Route::post('/edit/{id}', ['as' => 'panel.page.update', 'uses' => 'Panel\PanelPagesController@update']);
    });

    Route::group(['prefix' => 'player'], function () {
        Route::get('/', ['as' => 'panel.player', 'uses' => 'Panel\PanelPlayerController@index']);
        Route::post('/', ['as' => 'panel.player.store', 'uses' => 'Panel\PanelPlayerController@store']);
        Route::get('/create', ['as' => 'panel.player.create', 'uses' => 'Panel\PanelPlayerController@create']);
        Route::get('/edit/{id}', ['as' => 'panel.player.edit', 'uses' => 'Panel\PanelPlayerController@edit']);
        Route::get('/delete/{id}', ['as' => 'panel.player.destroy', 'uses' => 'Panel\PanelPlayerController@destroy']);
        Route::post('/edit/{id}', ['as' => 'panel.player.update', 'uses' => 'Panel\PanelPlayerController@update']);
    });

    Route::group(['prefix' => 'team'], function () {
        Route::get('/our', ['as' => 'panel.team.our', 'uses' => 'Panel\PanelTeamController@indexOur']);
        Route::get('/enemy', ['as' => 'panel.team.enemy', 'uses' => 'Panel\PanelTeamController@indexEnemy']);
        Route::post('/', ['as' => 'panel.team.store', 'uses' => 'Panel\PanelTeamController@store']);
        Route::get('/create', ['as' => 'panel.team.create', 'uses' => 'Panel\PanelTeamController@create']);
        Route::get('/edit/{id}', ['as' => 'panel.team.edit', 'uses' => 'Panel\PanelTeamController@edit']);
        Route::get('/delete/{id}', ['as' => 'panel.team.destroy', 'uses' => 'Panel\PanelTeamController@destroy']);
        Route::post('/edit/{id}', ['as' => 'panel.team.update', 'uses' => 'Panel\PanelTeamController@update']);
    });


    Route::group(['prefix' => 'arena'], function () {
        Route::get('/', ['as' => 'panel.arena', 'uses' => 'Panel\PanelArenaController@index']);
        Route::post('/', ['as' => 'panel.arena.store', 'uses' => 'Panel\PanelArenaController@store']);
        Route::get('/create', ['as' => 'panel.arena.create', 'uses' => 'Panel\PanelArenaController@create']);
        Route::get('/edit/{id}', ['as' => 'panel.arena.edit', 'uses' => 'Panel\PanelArenaController@edit']);
        Route::get('/delete/{id}', ['as' => 'panel.arena.destroy', 'uses' => 'Panel\PanelArenaController@destroy']);
        Route::post('/edit/{id}', ['as' => 'panel.arena.update', 'uses' => 'Panel\PanelArenaController@update']);
    });


    Route::group(['prefix' => 'calendar'], function () {
        Route::get('/', ['as' => 'panel.calendar', 'uses' => 'Panel\PanelCalendarController@index']);
        Route::post('/', ['as' => 'panel.calendar.store', 'uses' => 'Panel\PanelCalendarController@store']);
        Route::get('/create', ['as' => 'panel.calendar.create', 'uses' => 'Panel\PanelCalendarController@create']);
        Route::get('/up/{id}', ['as' => 'panel.calendar.up', 'uses' => 'Panel\PanelCalendarController@up']);
        Route::get('/down/{id}', ['as' => 'panel.calendar.down', 'uses' => 'Panel\PanelCalendarController@down']);
        Route::get('/edit/{id}', ['as' => 'panel.calendar.edit', 'uses' => 'Panel\PanelCalendarController@edit']);
        Route::post('/edit/{id}', ['as' => 'panel.calendar.update', 'uses' => 'Panel\PanelCalendarController@update']);
        Route::get('/delete/{id}', ['as' => 'panel.calendar.destroy', 'uses' => 'Panel\PanelCalendarController@destroy']);
        Route::get('/{id}', ['as' => 'panel.calendar.match', 'uses' => 'Panel\PanelMatchController@index']);
    });

    Route::group(['prefix' => 'match'], function () {
        Route::get('/', ['as' => 'panel.match', 'uses' => 'Panel\PanelMatchController@index']);
        Route::post('/', ['as' => 'panel.match.store', 'uses' => 'Panel\PanelMatchController@store']);
        Route::get('/create', ['as' => 'panel.match.create', 'uses' => 'Panel\PanelMatchController@create']);
        Route::get('/create2/{id}', ['as' => 'panel.match.create2', 'uses' => 'Panel\PanelMatchController@create2']);
        Route::post('/create2/{id}', ['as' => 'panel.match.store2', 'uses' => 'Panel\PanelMatchController@store2']);
        Route::get('/create3/{id}', ['as' => 'panel.match.create3', 'uses' => 'Panel\PanelMatchController@create3']);
        Route::post('/create3/{id}', ['as' => 'panel.match.store3', 'uses' => 'Panel\PanelMatchController@store3']);
        Route::get('/finish/{id}', ['as' => 'panel.match.finish', 'uses' => 'Panel\PanelMatchController@finish']);
        Route::post('/finish/{id}', ['as' => 'panel.match.finish_end', 'uses' => 'Panel\PanelMatchController@finish_end']);
        Route::get('/photo/{id}', ['as' => 'panel.match.photo', 'uses' => 'Panel\PanelMatchController@photo']);
        Route::post('/photo/{id}', ['as' => 'panel.match.photo_store', 'uses' => 'Panel\PanelMatchController@photo_store']);
        Route::get('/delete/{id}', ['as' => 'panel.match.destroy', 'uses' => 'Panel\PanelMatchController@destroy']);
        Route::get('/edit/{id}', ['as' => 'panel.match.edit', 'uses' => 'Panel\PanelMatchController@edit']);
        Route::post('/edit/{id}', ['as' => 'panel.match.update', 'uses' => 'Panel\PanelMatchController@update']);
    });

    Route::group(['prefix' => 'table'], function () {
        Route::get('/', ['as' => 'panel.table', 'uses' => 'Panel\PanelTableController@index']);
        Route::post('/', ['as' => 'panel.table.store', 'uses' => 'Panel\PanelTableController@store']);
        Route::get('/create', ['as' => 'panel.table.create', 'uses' => 'Panel\PanelTableController@create']);
        Route::get('/delete/{id}', ['as' => 'panel.table.destroy', 'uses' => 'Panel\PanelTableController@destroy']);
        Route::get('/edit/{id}', ['as' => 'panel.table.edit', 'uses' => 'Panel\PanelTableController@edit']);
        Route::post('/edit/{id}', ['as' => 'panel.table.update', 'uses' => 'Panel\PanelTableController@update']);
        Route::get('/up/{id}', ['as' => 'panel.table.up', 'uses' => 'Panel\PanelTableController@up']);
        Route::get('/down/{id}', ['as' => 'panel.table.down', 'uses' => 'Panel\PanelTableController@down']);
        Route::get('/update_data/{id}', ['as' => 'panel.table.update_data', 'uses' => 'Panel\PanelTableController@update_data']);
    });
    Route::group(['prefix' => 'photoalbum'], function () {
        Route::get('/', ['as' => 'panel.photoalbum', 'uses' => 'Panel\PanelPhotoalbumController@index']);
        Route::post('/', ['as' => 'panel.photoalbum.store', 'uses' => 'Panel\PanelPhotoalbumController@store']);
        Route::post('/upload', ['as' => 'panel.photoalbum.upload', 'uses' => 'Panel\PanelPhotoalbumController@upload']);
        Route::get('/create', ['as' => 'panel.photoalbum.create', 'uses' => 'Panel\PanelPhotoalbumController@create']);
        Route::get('/{id}', ['as' => 'panel.photoalbum.show', 'uses' => 'Panel\PanelPhotoalbumController@show']);
        Route::get('/delete/{id}', ['as' => 'panel.photoalbum.destroy', 'uses' => 'Panel\PanelPhotoalbumController@destroy']);
        Route::get('/delete_photo/{id}', ['as' => 'panel.photoalbum.destroy_photo', 'uses' => 'Panel\PanelPhotoalbumController@destroy_photo']);
        Route::post('/edit/{id}', ['as' => 'panel.photoalbum.update', 'uses' => 'Panel\PanelPhotoalbumController@update']);
        Route::get('/edit/{id}', ['as' => 'panel.photoalbum.edit', 'uses' => 'Panel\PanelPhotoalbumController@edit']);
    });

    Route::group(['prefix' => 'video'], function () {
        Route::get('/', ['as' => 'panel.video', 'uses' => 'Panel\PanelVideoController@index']);
        Route::post('/', ['as' => 'panel.video.store', 'uses' => 'Panel\PanelVideoController@store']);
        Route::get('/create', ['as' => 'panel.video.create', 'uses' => 'Panel\PanelVideoController@create']);
        Route::get('/delete/{id}', ['as' => 'panel.video.destroy', 'uses' => 'Panel\PanelVideoController@destroy']);
        Route::post('/edit/{id}', ['as' => 'panel.video.update', 'uses' => 'Panel\PanelVideoController@update']);
        Route::get('/edit/{id}', ['as' => 'panel.video.edit', 'uses' => 'Panel\PanelVideoController@edit']);
    });
});


Route::get('/{slug}', ['as' => 'page.show', 'uses' => 'PagesController@show']);

