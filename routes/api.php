<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/',  ['as' => 'api', 'uses' => 'Panel\PanelTeamController@apiShow']);
Route::get('/team/{id}',  ['as' => 'api.team', 'uses' => 'Panel\PanelTeamController@apiShow']);
Route::post('/bestplayers',  ['as' => 'api.bestplayers', 'uses' => 'MainController@bestplayers']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
